# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import sys
from functools import reduce
from operator import add
from shutil import copyfile
import time
import subprocess
from scipy.stats import invgamma, norm

import numpy as np 
import nifty7 as ift
import src as grav

from mpi4py import MPI


if __name__ == '__main__':

    start_time = time.time()
    commMPI = MPI.COMM_WORLD
    rankMPI = commMPI.Get_rank()
    
    # 1. IN- AND OUTPUTS
    # ------------------
    ifile = sys.argv[1]
    ofile = sys.argv[2]

    # location of the source code
    srcloc =  __file__.split('/')[:-1]
    srcloc = [sl+'/' for sl in srcloc]
    if len(srcloc) > 0:
        srcloc = reduce(add,srcloc)
    else:
        srcloc = ""

    # configuration file: check if provided as user argument
    # if not, read from default location
    try:
        cfile = sys.argv[3]
    except:
        cfile  = srcloc+'config.py'
        
    exec(open(cfile).read())

    # save the configuration file to the output location
    copyfile(cfile, ofile+'_config.py')
    if rankMPI==0:
        with open(ofile+'_config.py', 'a') as ff:
            ntime  = time.strftime('%c')
            ff.write('# run started on ' + ntime + '\n')
            ff.write('# data file used: ' + ifile + '\n')
            try:
                ghash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode("utf-8")
                ff.write('# git hash: ')
                ff.write(ghash)
                ff.write('\n')
            except:
                ff.write('# could not identify git hash.\n')
                
        
    # load the sampling routine
    iroutine = 'sampling_routines.routine{:d}'.format(sampling_routine)
    sampling_routine = getattr(__import__(iroutine, fromlist=['sampling_routine']), 'sampling_routine')

    #load the constants routine
    iroutine = 'sampling_routines.constants{:d}'.format(constants_routine)
    const_during_minimization = getattr(__import__(iroutine, fromlist=['const_during_minimization']),\
                                        'const_during_minimization')

    # expand path to the aberration maps
    cmap_file = srcloc + 'calibration/aberration-maps/' + cmap_file

    # load the data
    data = {}
    if rankMPI==0:
        data = grav.read_data(ifile, min_channel=min_channel, max_channel=max_channel, polarization=polarization,\
                              trange=time_range, idx_frame_exclude=idx_frame_exclude, insname=insname)
        grav.plot_uv_coverage(data, ofile+'_uv_coverage.png')
        grav.message_data_selection(data)
        grav.message_data_selection_closure(data, data_use_closure)
    data = commMPI.bcast(data, root=0)

    # upscale error bars 
    data['spi'] = sigma_phi_scale*data['spi']
    data['sro'] = sigma_amp_scale*data['sro']
    data['t3e'] = sigma_t3p_scale*data['t3e']
        
    if rankMPI==0:
        grav.message_fiber_position(data)
        grav.message_time_binning(data, tmax, time_range)
        
    # 2. RESPONSE
    # -----------

    # extra (= non-variable) point sources
    # routines need to read/write to the phasemaps so execute on a single thread
    extra_ps_n         = len(extra_ps_coord)
    extra_ps_injection = [[],]*data['nbl']
    extra_ps_flux_keys = []
    extra_ps_pos_keys  = []
    extra_ps_response  = []
    if extra_ps_n > 0 and rankMPI==0:
        # if present, the cental ps has index 1 so here we count from 2 upwards
        extra_ps_flux_keys = ['source{:d}_flux'.format(ips+2) for ips in range(extra_ps_n)]
        extra_ps_pos_keys  = [['source{:d}_ra'.format(ips+2), 'source{:d}_dec'.format(ips+2)] for ips in range(extra_ps_n)]
        extra_ps_injection = np.full( [data['nbl'], extra_ps_n, 2, data['nlam']], np.nan)
        cmap_param         = [image_domain, cmap_file, 1., phasemap_clip_low]
        for ips in range(extra_ps_n):
            rtmp, itmp = grav.point_source_visibility(data, extra_ps_coord[ips], bw_resolution, extra_ps_pos_keys[ips],\
                                                      *cmap_param, data_use_closure)
            extra_ps_response.append(rtmp)
            extra_ps_injection[:,ips,:,:] = itmp
        extra_ps_response = np.array(extra_ps_response)
        extra_ps_pos_keys = np.array(extra_ps_pos_keys)
    extra_ps_coord    = np.array(extra_ps_coord)
    extra_ps_injection = commMPI.bcast(extra_ps_injection, root=0)
    extra_ps_flux_keys = commMPI.bcast(extra_ps_flux_keys, root=0)
    extra_ps_pos_keys  = commMPI.bcast(extra_ps_pos_keys, root=0)
    extra_ps_response  = commMPI.bcast(extra_ps_response, root=0)

    # define domains
    dom_rr     = ift.DomainTuple.make( [image_domain, ift.UnstructuredDomain(data['nlam'])])
    dom_alpha  = ift.ContractionOperator(ift.UnstructuredDomain(data['nlam']), spaces=0).target
    dom_lh     = ift.DomainTuple.make( [ift.UnstructuredDomain(1 + int(data_use_closure)*int(np.sum(data['closures']==0))),\
                                        ift.UnstructuredDomain(data['nframe']),\
                                        ift.UnstructuredDomain(data['npol']),\
                                        ift.UnstructuredDomain(data['nlam'])] )
    dom_bw     = ift.DomainTuple.make( [ift.UnstructuredDomain(1 + int(data_use_closure)*int(np.sum(data['closures']==0))),\
                                        ift.UnstructuredDomain(data['nframe']),\
                                        ift.UnstructuredDomain(data['npol']),\
                                        ift.UnstructuredDomain(data['nlam']*bw_resolution)] )

    
    # define the  multi-domain for the full model
    dom_sky    = {'sky_alpha2' : image_domain, 'alpha1' : dom_alpha, 'alpha2' : dom_alpha, 'flux_bkg' : dom_alpha,\
                  'alpha_bkg' : dom_alpha}
    dom_sky['source1_ra']  = dom_alpha
    dom_sky['source1_dec'] = dom_alpha
    for ips in range(extra_ps_n):
        dom_sky[extra_ps_pos_keys[ips,0]] = dom_alpha
        dom_sky[extra_ps_pos_keys[ips,1]] = dom_alpha
        dom_sky[extra_ps_flux_keys[ips]]  = dom_alpha
            
    if free_scale_bl == True:
        key_base = 'xi_scale_bl{:d}'
        for ibl in range(data['nbl']):
                dom_sky[key_base.format(ibl)] = ift.UnstructuredDomain(data['nframe'])
        
    # specific to the first polarization
    dom_sky_p1 = dom_sky.copy()
    dom_sky_p1['variability_p1'] = ift.UnstructuredDomain(data['nframe'])
    
    # specific to the second polarization
    dom_sky_p2 = dom_sky.copy()
    dom_sky_p2['variability_p2'] = ift.UnstructuredDomain(data['nframe'])
    
    # create multi-domains
    dom_sky    = ift.MultiDomain.make({**dom_sky_p1, **dom_sky_p2})
    dom_sky_p1 = ift.MultiDomain.make(dom_sky_p1)
    dom_sky_p2 = ift.MultiDomain.make(dom_sky_p2)
    
    # operators which do not depend on baseline
    explam = ift.ContractionOperator(dom_rr, spaces=1).adjoint

    # central, variable point-source
    ps_fiber_injection = [None]*data['nbl']
    rr0 = None
    if rankMPI==0:
        cmap_param = [image_domain, cmap_file, 1., phasemap_clip_low]
        rr0, ps_fiber_injection = grav.point_source_visibility(data, sgra_coord_mean, bw_resolution,\
                                                               ['source1_ra', 'source1_dec'], *cmap_param, data_use_closure)
        ps_fiber_injection *= sgra_point_scale
    ps_fiber_injection = commMPI.bcast(ps_fiber_injection, root=0)
    rr0 = commMPI.bcast(rr0, root=0)
    
    # lists to hold baseline-dependent operators
    rr_bl  = np.empty((2, data['nbl']), dtype=grav.PhasemapResponse)
    sn_bl  = np.empty((data['npol'], data['nbl']), dtype=grav.NormOp_PmLSB)


    # assign baseline dependent operators
    # do this on a single task and then distribute the result to avoid read/write conflicts with the phasemaps
    if rankMPI==0:
        for ibl in range(data['nbl']):

            # visibility normalization
            kwargs_norm = {'clip_low'  : phasemap_clip_low,\
                           'injection' : ps_fiber_injection[ibl],\
                           'extra_ps_flux_keys' : extra_ps_flux_keys,\
                           'extra_ps_injection' : extra_ps_injection[ibl],\
                           'phasemap_kernel'    : phasemap_kernel_stddev}
            sn_bl[:,ibl] = grav.get_NormOp_bl('deterministic', data, ibl, [dom_sky_p1, dom_sky_p2], cmap_file, kwargs_norm)

            # interferometric equation: rr implements the fourier transform
            rr = grav.PhasemapResponse(dom_rr, data, bw_resolution, 1e-4, cmap_file, ibl, 1., clip_low=phasemap_clip_low,\
                                       use_closure=data_use_closure, phasemap_kernel=phasemap_kernel_stddev)

            # response w/ variable spectral index
            spec1 = grav.make_variable_spectrum_op(data, bw_resolution, ibl, dom_sky, 'alpha1', use_closure=data_use_closure)
            spec2 = grav.make_variable_spectrum_op(data, bw_resolution, ibl, dom_sky, 'alpha2', use_closure=data_use_closure)
            rr_bl[0,ibl] = spec1* sgra_point_scale *rr0[ibl]
            rr_bl[1, ibl] = rr @ explam @ ift.ducktape(image_domain, dom_sky, 'sky_alpha2')

            # add external point sources
            for ips in range(extra_ps_n):
                expvis  = ift.ContractionOperator(spec2.target, spaces=(0,1,2)).adjoint
                flux_op = expvis @ ift.FieldAdapter(dom_alpha, extra_ps_flux_keys[ips])
                rr_bl[1,ibl] = rr_bl[1,ibl] + (flux_op* extra_ps_response[ips,ibl])
            rr_bl[1,ibl] = spec2* rr_bl[1,ibl]
            
    rr_bl = commMPI.bcast(rr_bl, root=0)
    sn_bl = commMPI.bcast(sn_bl, root=0)

    # plot phase and amplitude maps
    if rankMPI==0:
        idx_lam = data['nlam']//2
        aberration_ops = [grav.PhasemapResponse(dom_rr, data, bw_resolution, 1e-4, cmap_file, ibl, 1., clip_low=phasemap_clip_low).\
                          _phase_screen[idx_lam, 0] for ibl in [0, 3, 5, 5]]
        aberration_ops[-1] = aberration_ops[-1].conjugate()
        grav.plot_aberration_op(aberration_ops, ofile, fov)

    # assemble the full visibility    
    exppol_1 = ift.ContractionOperator(dom_bw, spaces=(0,2,3)).adjoint
    exppol_2 = ift.ContractionOperator(dom_bw, spaces=2).adjoint
    exppol_3 = ift.ContractionOperator(dom_bw, spaces=(0,2)).adjoint

    # operator to multiply each polarization state by its lightcurve
    flux     = exppol_1 @ ift.FieldAdapter(dom_sky['variability_p1'], 'variability_p1')
    if data['npol'] == 2:
        # make sure that each lightcurve is applied to the correct polarization state
        mask_p1 = np.zeros(dom_bw.shape)
        mask_p1[:, :, 0,:] = 1.
        mask_p1 = ift.makeOp(ift.makeField(dom_bw, mask_p1))
        mask_p2 = np.zeros(dom_bw.shape)
        mask_p2[:, :, 1,:] = 1
        mask_p2 = ift.makeOp(ift.makeField(dom_bw, mask_p2))
        flux    = (mask_p1 @ exppol_1 @ ift.FieldAdapter(dom_sky['variability_p1'], 'variability_p1')) +\
                  (mask_p2 @ exppol_1 @ ift.FieldAdapter(dom_sky['variability_p2'], 'variability_p2'))

    # put together the visibility
    vis = [ exppol_2 @ rr_bl[1,ibl] + flux*(exppol_2 @ rr_bl[0,ibl]) for ibl in range(data['nbl'])]

    # apply bandwidth smearing/average over sub-channel resolution
    bws = grav.BWS_Average(dom_bw, bw_resolution)
    vis = [ bws @ vv for vv in vis]
        
    # combine the normalization operators for both polarizations
    exppol = ift.ContractionOperator(dom_lh, spaces=(0,2)).adjoint
    if data['npol'] == 2:
        # make sure that each lightcurve is applied to the correct polarization state
        mask_p1 = np.zeros(dom_lh.shape)
        mask_p1[:, :, 0,:] = 1.
        mask_p1 = ift.makeOp(ift.makeField(dom_lh, mask_p1))
        mask_p2 = np.zeros(dom_lh.shape)
        mask_p2[:, :, 1,:] = 1
        mask_p2 = ift.makeOp(ift.makeField(dom_lh, mask_p2))
        sn_bl = [(mask_p1 @ exppol @ sn_bl[0,ibl]) + (mask_p2 @ exppol @ sn_bl[1,ibl]) for ibl in range(data['nbl'])]
    else:
        sn_bl = [ exppol @ sn_bl[0,ibl] for ibl in range(data['nbl'])]


    # apply normalization to the visibilities
    vis = [ sn_bl[ibl]*vis[ibl] for ibl in range(data['nbl']) ]

    # apply amplitude calibration
    if free_scale_bl == True:
        expop = ift.ContractionOperator(dom_lh, spaces=(0,2,3)).adjoint.real
        scale_bl = grav.amplitude_calibration(data, data_use_closure, sigma=free_scale_bl_sigma, key_base=key_base)
        vis = [(expop @ scale_bl[ibl])*vis[ibl] for ibl in range(data['nbl'])]

    # crosscheck individual operators
    if check_operators == True:
        print()
        print('Checking Operators...')
        ift.extra.check_linear_operator(bws)
        print('...Bandwidth smearing successfull.')
        ift.extra.check_linear_operator(rr, target_dtype=np.complex128, only_r_linear=True, rtol=1e-5)
        print('...PhasemapResponse successfull.')
        ift.extra.check_operator(sn_bl[0], ift.from_random(sn_bl[0].domain), ntries=10, tol=1e-5)
        print('...PhasemapNormalization successfull.')
        ift.extra.check_operator(spec1, ift.from_random(spec1.domain), ntries=10)
        print('...Spectrum Operator Successfull')
        phase_screen  = grav.phasemaps._aberration_op_fromdata(image_domain, data, cmap_file, 0, 1, 2.2e-6)
        ii_lobe = [ift.makeOp(ift.makeField(image_domain, np.real(pp.conj()*pp) )) for pp in phase_screen[:,0] ]
        ift.extra.check_linear_operator(grav.response.SpecialInt(ii_lobe))
        print('...SpecialInt successfull.')
        ift.extra.check_operator(extra_ps_response[-1,3], ift.from_random(extra_ps_response[-1,3].domain), ntries=10)
        print('...Point source response successfull.')
        print()

    # 3. SKY MODEL
    #-------------    
    # 3.a) spatial distributions

    invG_param['domain'] = image_domain
    sky_alpha2 = ift.InverseGammaOperator(**invG_param) @ ift.FieldAdapter(image_domain, 'xi_sky_alpha2')
    sky = ift.ducktape(dom_sky, image_domain, 'sky_alpha2') @ sky_alpha2
    sky_lam0  = sky_alpha2
    ps_ops = []
    
    # add Sgr A* to the sky model
    pos_scale = ift.ScalingOperator(dom_alpha, sgra_coord_std)
    ra_op     = ift.Adder(ift.full(dom_alpha, sgra_coord_mean[0])) @ pos_scale @ ift.FieldAdapter(dom_alpha, 'xi_source1_ra').real
    dec_op    = ift.Adder(ift.full(dom_alpha, sgra_coord_mean[1])) @ pos_scale @ ift.FieldAdapter(dom_alpha, 'xi_source1_dec').real
    sky = sky + ift.ducktape(dom_sky, dom_alpha, 'source1_ra')  @ ra_op
    sky = sky + ift.ducktape(dom_sky, dom_alpha, 'source1_dec') @ dec_op
    flux_op   = ift.ScalingOperator(dom_alpha, sgra_point_scale) @ ift.ScalingOperator(dom_alpha, 0.).exp() @ \
                ift.FieldAdapter(dom_alpha, 'xi_source1_dec').real # hack to get a constant operator
    ps_ops.append([ra_op, dec_op, flux_op])

    # add extra point sources
    for ips in range(extra_ps_n):
        pos_scale = ift.ScalingOperator(dom_alpha, extra_ps_sigma[ips])
        ra_op     = ift.Adder(ift.full(dom_alpha, extra_ps_coord[ips,0])) @ pos_scale @\
                    ift.FieldAdapter(dom_alpha, 'xi_'+extra_ps_pos_keys[ips,0]).real
        dec_op    = ift.Adder(ift.full(dom_alpha, extra_ps_coord[ips,1])) @ pos_scale @\
                    ift.FieldAdapter(dom_alpha, 'xi_'+extra_ps_pos_keys[ips,1]).real
        flux_op   = ift.FieldAdapter(dom_alpha, 'xi_'+extra_ps_flux_keys[ips]).real.exp()
        
        sky = sky + ift.ducktape(dom_sky, dom_alpha,  extra_ps_pos_keys[ips,0]) @ ra_op
        sky = sky + ift.ducktape(dom_sky, dom_alpha,  extra_ps_pos_keys[ips,1]) @ dec_op
        sky = sky + ift.ducktape(dom_sky, dom_alpha,  extra_ps_flux_keys[ips] ) @ flux_op

        ps_ops.append( [ra_op, dec_op, flux_op])
    ps_ops = np.array(ps_ops)
        
    # 3.b) lightcurve

    # uncorrelated frames
    frames = ift.ScalingOperator(ift.UnstructuredDomain(data['nframe']), 1.)
    time_corr_p1 = ift.FieldAdapter(dom_sky['variability_p1'], 'xi_variability_p1').exp()
    time_corr_p2 = ift.FieldAdapter(dom_sky['variability_p2'], 'xi_variability_p2').exp()

    variability = ift.ducktape(dom_sky, dom_sky['variability_p1'], 'variability_p1') @ frames @ time_corr_p1.real
    brightness  = [time_corr_p1]
    if data['npol']==2:
        variability = variability + ift.ducktape(dom_sky, dom_sky['variability_p2'], 'variability_p2') @ frames @ time_corr_p2.real
        brightness.append(time_corr_p2)
            
    # 3.c) full sky
    sky    = sky + variability

    # 3.d) spectral indices
    alpha1_op = ift.FieldAdapter(dom_alpha, 'xi_alpha1').real
    alpha1_op = ift.ScalingOperator(dom_alpha, alpha1_var) @ alpha1_op
    alpha1_op = ift.Adder(ift.makeField(dom_alpha, alpha1)) @ alpha1_op
        
    alpha2_op = ift.FieldAdapter(dom_alpha, 'xi_alpha2').real
    alpha2_op = ift.ScalingOperator(dom_alpha, alpha2_var) @ alpha2_op
    alpha2_op = ift.Adder(ift.makeField(dom_alpha, alpha2)) @ alpha2_op
        
    sky = sky + ift.ducktape(dom_sky, dom_alpha, 'alpha1') @ alpha1_op
    sky = sky + ift.ducktape(dom_sky, dom_alpha, 'alpha2') @ alpha2_op

    # 3.e) background flux
    alpha_bg_op = ift.FieldAdapter(dom_alpha, 'xi_alpha_bkg').real
    alpha_bg_op = ift.ScalingOperator(dom_alpha, alpha_bg_var)  @ alpha_bg_op
    alpha_bg_op = ift.Adder(ift.makeField(dom_alpha, alpha_bg)) @ alpha_bg_op

    flux_bg_op = ift.FieldAdapter(dom_alpha, 'xi_flux_bkg').real
    flux_bg_op = ift.ScalingOperator(dom_alpha, bkg_var) @ flux_bg_op
    flux_bg_op = ift.Adder(ift.makeField(dom_alpha, bkg_level)) @ flux_bg_op
    flux_bg_op = flux_bg_op.clip(a_min=0., a_max=None)

    sky = sky + ift.ducktape(dom_sky, dom_alpha, 'alpha_bkg') @ alpha_bg_op
    sky = sky + ift.ducktape(dom_sky, dom_alpha, 'flux_bkg') @ flux_bg_op

    # 3.f) free amplitude scaling per baseline
    if free_scale_bl:
        for sb in scale_bl:
            name = sb.domain.keys()[0]
            sky = sky + ift.ducktape(dom_sky, dom_sky[name], name) @ ift.FieldAdapter(dom_sky[name], name)
       

    # 4. LIKELIHOOD
    # -------------
    vis = [vv @ sky for vv in vis]

    # organize the visibilities in a Multi-Field
    # naming convention = bl-{idx_tel1}-{idx_tel2} w/ telescope indices staring at 0 as in data['baselines']
    # this convention is also assumed by the likelihood
    bl_names = ['bl-{:d}-{:d}'.format(tel[0], tel[1]) for tel in data['baselines']]
    mdom_lh  = {nn : dom_lh for nn in bl_names}

            
    # get the likelihood
    lhs = grav.get_likelihood(data, lh_type, data_selection)    
    vis_lh = [ift.ducktape(mdom_lh, dom_lh, bl_names[ii]) @ vis[ii] for ii in range(data['nbl'])]
    vis_lh = reduce(add, vis_lh)
    lh     = reduce(add, lhs)
    lh     = lh @ vis_lh
    
    # 5. INITIAL POSITION
    # -------------------
    # -------------------

    initial_position, n_it_start = grav.initial_conditions(vis_lh.domain, ini_type, restart_file=restart_file)

    # plot the inital position
    if rankMPI==0 and n_it_start==0:
        ps_pos_flux = np.full(ps_ops.shape, np.nan)
        for ips in range(ps_ops.shape[0]):
            for jj in range(ps_ops.shape[1]):
                ps_pos_flux[ips,jj] = ps_ops[ips,jj].force(initial_position).val
        grav.plot_data_comparison(vis_lh(initial_position), data, data_selection, outname=ofile+'_v-ini')
        grav.plot_image(sky_lam0.force(initial_position), ofile+'_i-ini', fov=fov, logscale=True, extra_ps=ps_pos_flux)
        grav.plot_brightness_evolution([bb.force(initial_position) for bb in brightness], ofile+'_b-ini', tmax , times=data['tim'])
            
    # 6. Minimization
    # ---------------
    # ---------------
    pos = initial_position

    
    # define the iteration controller which is passed to the hamiltonian
    assert iteration_controller in ['gradient','delta'],\
        "Requested iteration controller type {} not available".format(iteration_controller)
    if iteration_controller=='gradient':
        it_ctrl_h = ift.GradientNormController(iteration_limit=40, name='it_ctrl_h')
    elif iteration_controller=='delta':
        it_ctrl_h = ift.AbsDeltaEnergyController(delta_it_ctrl_h, convergence_level=2, iteration_limit=200, name='it_ctrl_h')

    # define the minimizer for KL optimization
    assert minimizer_type in ['NewtonCG', 'VL_BFGS'], "Requested minimizer type {} not available".format(minimizer_type)
    if minimizer_type=='NewtonCG':
        minOp = ift.NewtonCG
    elif minimizer_type=='VL_BFGS':
        minOp = ift.VL_BFGS

    # output
    if rankMPI==0:
        print(' ')
        print('----------')
        print(' ')

    # random seed -> affects MGVI samples
    if random_seed != None:
        ift.random.push_sseq_from_seed(random_seed)

    for jj in range(n_it_start, n_iterations_mgvi):

        # minimization parameters
        pos_prev   = pos
        constants  = const_during_minimization(jj, pos)
        
        # if the central source flux scale is zero, fix the lightcurve (it has no effect on the visibilities anyway)
        # this way you can disable Sgr A* and image a static field
        if sgra_point_scale==0.0:
            if 'xi_variability_p1' in pos.keys() and 'xi_variability_p1' not in constants:
                constants.append('xi_variability_p1')
            if 'xi_variability_p2' in pos.keys() and 'xi_variability_p2' not in constants:
                constants.append('xi_variability_p2')

        n_samp, n_it = sampling_routine(jj)

        # likelihood & hamiltonian
        hamiltonian = ift.StandardHamiltonian(lh, it_ctrl_h)

        # defining the KL
        KL = ift.MetricGaussianKL(pos, hamiltonian, n_samp, mirror_samples=True, comm=commMPI, nanisinf=False, constants=constants,\
                                      point_estimates=constants)
            
        # set up the minimizer
        dct         = {'deltaE': delta_it_ctrl_m, 'iteration_limit': n_it, 'convergence_level': 2, 'name':'it_ctrl_min'}
        minimizer   = minOp(ift.AbsDeltaEnergyController(**dct))

        KL, _       = minimizer(KL)
        pos         = KL.position
        pos         = ift.MultiField.from_dict({**pos_prev.to_dict(), **pos.to_dict()}, domain=lh.domain)
        samp_tmp    = ift.full(lh.domain, 0.).to_dict()
        samples     = [ift.MultiField.from_dict({**ift.full(lh.domain, 0.).to_dict(), **ss.to_dict()}, domain=lh.domain)\
                       for ss in KL.samples]

        # saving and plotting
        if rankMPI == 0:

            # evaluate posterior samples
            sc_image = ift.StatCalculator()
            sc_vis   = ift.StatCalculator()
            sc_err   = [ift.StatCalculator() for ds in data_selection]
            sc_ps    = [ift.StatCalculator() for ips in range(3*len(ps_ops))]
            if free_scale_bl:
                sc_scale = [ift.StatCalculator() for ibl in range(data['nbl'])]
            
            for ss in samples:
                sc_image.add(sky_lam0.force(pos+ss))
                sc_vis.add(vis_lh.force(pos+ss))
                if lh_type=='error_scaling':
                    for ii, dd in enumerate(data_selection):
                        sc_err[ii].add(error_scale_op(pos[dd+'_err_fac']+ss[dd+'_err_fac']))
                for ips in range(ps_ops.shape[0]):
                    for pp in range(ps_ops.shape[1]):
                        sc_ps[3*ips+pp].add(ps_ops[ips,pp].force(pos+ss))
                if free_scale_bl:
                    for ibl in range(data['nbl']):
                        sc_scale[ibl].add(scale_bl[ibl].force(pos+ss))

            # get some means
            ps_pos_flux = np.full(ps_ops.shape, np.nan)
            for ips in range(ps_ops.shape[0]):
                for pp in range(ps_ops.shape[1]):
                    ps_pos_flux[ips,pp] = sc_ps[3*ips+pp].mean.val
                    
            # plotting
            grav.save_position(pos, ofile+'_p{:03d}'.format(jj+1), samples=samples)
            grav.plot_brightness_evolution_samples(brightness, pos, samples, tmax, times=data['tim'],\
                                                   outname=ofile+'_b{:03d}'.format(jj+1))
            grav.plot_image(sc_image.mean, ofile+'_i{:03d}'.format(jj+1), fov=fov, logscale=True, extra_ps=ps_pos_flux)
            grav.plot_data_comparison(sc_vis.mean, data, data_selection, outname=ofile+'_v{:03d}'.format(jj+1))
            if free_scale_bl:
                scales_mean_tmp = np.array([sbl.mean for sbl in sc_scale] )
                scales_std_tmp  = np.array([sbl.var.sqrt() for sbl in sc_scale])
                grav.plot_scale_bl(scales_mean_tmp, ofile+'_scale-{:03d}'.format(jj+1), data, var=scales_std_tmp)
            
            grav.message_progress(jj, sampling_routine, 'mgvi', constants)

    # 7. SAVE THE RESULT & CLEAN UP
    # -----------------------------
        
    if rankMPI == 0:
        grav.message_final_information(ofile, start_time)
