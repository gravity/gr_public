# GRAVITY_RESOLVE ($`\mathrm{G^R}`$)

GRAVITY_RESOLVE ($`\mathrm{G^R}`$) is a Bayesian imaging code for near-IR interferometry of the Galactic Center with GRAVITY.

## Dependencies
To use $`\mathrm{G^R}`$ the following programs and packages are requires
- [python3](https://www.python.org/) (>3.6)
- [scipy](https://scipy.org/)
- [NIFTy](https://gitlab.mpcdf.mpg.de/ift/nifty)
- [ducc0](https://gitlab.mpcdf.mpg.de/mtr/ducc)
- [mpi4py](https://mpi4py.scipy.org)
- [astropy](https://www.astropy.org/)
- [matplotlib](https://matplotlib.org/)

## Running the code

The most simple way to run the code is by the following syntax
```
python3 reconstruction.py /path/to/data/data.fits /path/to/results/outroot
```
This will read all parameters from the default configuration file `config.py`. An alternative configuration file can be supplied as optional fourth argument, i.e.
```
python3 reconstruction.py /path/to/data/data.fits /path/to/results/outroot alternative_config.py
```
The reconstruction file supports MPI parallelization, and the following command will run the reconstruction on `N` threads
```
mpirun -N python3 reconstruction.py /path/to/data/data.fits /path/to/results/outroot
```

A mock data set and the corresponding results are available for [download](https://datashare.mpcdf.mpg.de/s/uX0dcLCLpL2N5c1). To inspect the results, there is also a template jupyter notebook in the `notebooks` directory.

## References
- The GRAVITY imaging paper ["Deep Images of the Galactic Center with GRAVITY"](https://www.aanda.org/component/article?access=doi&doi=10.1051/0004-6361/202142459) explains in detail the scientific context and results, the modeling of GRAVITY and the Galactic Center and the choice of inference scheme and hyper-parameters.
- $`\mathrm{G^R}`$ draws from the experience on`RESOLVE` an Bayesian imaging code for radio-interferometry, see e.g. ["Comparison of classical and Bayesian imaging in radio interferometry"](https://www.aanda.org/articles/aa/full_html/2021/02/aa39258-20/aa39258-20.html) for details and ["M87* in space, time, and frequency"](https://arxiv.org/abs/2002.05218) for the application of `RESOLVE` to EHT data of M87.
- For a broader overview of Information Field Theory, see ["Information Theory for Fields"](https://onlinelibrary.wiley.com/doi/10.1002/andp.201800127).

## Licensing terms
$`\mathrm{G^R}`$ is licensed under the terms of the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and is distributed without any warranty.
