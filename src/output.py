# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift
import glob
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import os
from matplotlib.lines import Line2D

from .constants import MAS2RAD
from .likelihood import _data_mask, _closure_mask

def set_style(style):
    style_path = os.path.dirname(__file__) +'/'
    available_styles = [i[len(style_path):-9] for i in sorted(glob.glob(style_path+"*.mplstyle"))]
    if style in available_styles:
        chosen_style = style_path+style+'.mplstyle'
    elif style in plt.style.available:
        chosen_style = style
    else:
        raise ValueError("Style {} not found. Available styles: {}".format(style,", ".join(available_styles)))
    plt.rcParams.update(plt.rcParamsDefault)
    plt.style.use(chosen_style)

import colorsys
import matplotlib.colors as mc
def lighten_color(color, amount=0.5):
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

def get_uv_coordinates(data):
    """
    Get an array with uv-coordinates for visibilities and closures in the format nframe x nbl x nlam.
    For the closure phases, pick the longest baseline along each trinagle.

    Parameters:
    -----------
    data (dictionary)  : data dictionary as returned by src.data.read_data

    Returns:
    -------
    * uv_vis : visibility baselines in the format (nframe x nbl x nlam)
    * uv_t3p : longest baseline along each closure triangle in the format (nframe x ncl x nlam).
    """

    # get uv-coordinates
    uco     = np.full((data['nframe'], data['nbl'], data['nlam']), np.nan)
    vco     = np.full((data['nframe'], data['nbl'], data['nlam']), np.nan)
    for bl in range(data['nbl']):
        uco[:,bl,:] = np.outer(data['uco'][:,bl], 1./data['lam'])*MAS2RAD
        vco[:,bl,:] = np.outer(data['vco'][:,bl], 1./data['lam'])*MAS2RAD
    uv_vis = np.sqrt(uco**2 + vco**2)

    # for closures use the longest baseline in the triangle
    uv_t3p = np.full((data['nframe'], data['ncl'], data['nlam']), np.nan)
    for ii, idx_bls in enumerate(data['closures']):
        uv_t3p[:,ii,:] = np.amax([uv_vis[:,jj,:] for jj in idx_bls], axis=0)

    return uv_vis, uv_t3p
        
def plot_image(image, outname, fov=None, logscale=False, rel_min=1e-4, abs_max=None, extra_ps=[]):
    
    """Plot the inferred image
    Becasue of displaying conventions the image is mirrored along the RA axis which then runs from 
    positive to negative values.

    Parameters:
    -----------
    * image (ift.Field) : the image to be plotted
    * outname (string)  : file name to save the plot as png
    * fov (float, opt.)       : FOV depicted in mas assuming a quadratic image. If None axes won't be labelled
    * logscale (bool, opt.)   : weather to use logarithmic color sclaing
    * rel_min (float, opt.)   : lower cut on the color scale, relative to maximum flux in the image
    * abs_max (float, opt.) : upper cut to the color scale 
    * extra_ps (list, opt)  : point-sources to overplot on the image, for each source give (ra,dec, flux)

    Returns: the figure object
    -------
    """
    
    set_style('plotstyle')

    fig, ax = plt.subplots(1)

    pltargs = {'origin' : 'lower'}
    if fov != None:
        pltargs['extent'] = [fov/2, -fov/2, -fov/2, fov/2]
        ax.set_xlabel('RA [mas]')
        ax.set_ylabel('DEC [mas]')
    else:
        dim = image.val.shape[0]/2
        pltargs['extent'] = [dim, -dim, -dim, dim]
        ax.set_xlabel('RA [pixel]')
        ax.set_ylabel('DEC [pixel]')

    image2 = image.val.copy()
    
    # add extra point sources to the image
    spix   = image.domain[0].distances[0]
    npix_s = image.domain[0].shape[0]
    external_sources = []
    for ips in range(len(extra_ps)):
        idx = tuple(np.floor(extra_ps[ips,:2]/spix + npix_s/2).astype(int))
        if idx[0]<npix_s and idx[1]<npix_s:
            image2[idx] += extra_ps[ips,2]
        else:
            external_sources.append(extra_ps[ips])
    external_sources = np.array(external_sources)
        
    image2 = np.swapaxes(image2, 0, 1)
    image2 = np.flip(image2, axis=1)

    pltargs['norm'] = None
    if logscale:
        source_max = image2.max()
        image2 = np.clip(image2, source_max*rel_min, abs_max)
        pltargs['norm'] = colors.LogNorm(vmin=source_max*rel_min, vmax=abs_max)
        
    im = ax.imshow(image2, **pltargs)
    if len(external_sources) > 0:
        ax.scatter(external_sources[:,0], external_sources[:,1], c=external_sources[:,2], norm=pltargs['norm'])
    ax.set_aspect('equal')
    fig.tight_layout()

    cbar_ax = fig.add_axes([0.2, -0.05, 0.6, 0.02])
    cb = fig.colorbar(im, cax=cbar_ax, orientation='horizontal')
    cbar_ax.set_xlabel('brightness [arbitrary units]', fontsize='small')

    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig

def plot_brightness_evolution(timelines, outname, tmax, times=[]):

    """
    Plot lightcurves.

    Parameters:
    -----------
    * timelines (list) : iftFields containing the lightcurves
    * outname (string)  : file name to save the plot as png
    * tmax (double)     : time range for the plot
    * times (arra, opt) : time-coordinate corresponding to the values in the lightcurves
                          if not provided, equidistant time steps between 0 and tmax will be plotted

    Returns: the figure object
    -------
    """

    
    set_style('plotstyle')
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    if isinstance(timelines[0].domain[0], ift.RGSpace):
        tdat = np.linspace(0, tmax, timelines[0].domain.shape[0])
    elif times != []:
        tdat = times
    else:
        tdat = np.arange(0, timelines[0].domain.shape[0])

    # detect large gaps in the time array
    dtime = tdat[1:]-tdat[:-1]
    breakes = np.argwhere(dtime > 60.).flatten()
    nax = len(breakes) + 1
    limits = np.zeros( [nax,2])
    for ib in range(nax-1):
        limits[ib,1]   = tdat[breakes[ib]] + 5.0
        limits[ib+1,0] = tdat[breakes[ib]+1] -5.0
    limits[-1,1] = tdat[-1] + 5.0
    limits[0,0] = -5.0
        
    fig, ax = plt.subplots(1, nax, sharey=True)
    ax = np.array([ax,]).flatten()
    for it, tl in enumerate(timelines):
        for aa in ax:
            aa.plot(tdat, tl.val)

    # horizontal lines to indicate frames
    if len(times)>0:
        for aa in ax:
            [aa.axvline(tt, lw=.5, linestyle='--') for tt in times]


    # multi-plot formatting
    if nax>1:
        d = .015 
        for ia, aa in enumerate(ax):
            aa.set_xlim(*limits[ia])
            kwargs = dict(transform=aa.transAxes, color='k', clip_on=False)
            if ia<nax-1:
                aa.spines['right'].set_visible(False)
                aa.yaxis.tick_left()
                aa.plot((1-d,1+d), (-d,+d), **kwargs)
                aa.plot((1-d,1+d),(1-d,1+d), **kwargs)
            if ia>0:
                aa.spines['left'].set_visible(False)
                aa.yaxis.tick_right()
                aa.plot((-d,+d), (1-d,1+d), **kwargs)
                aa.plot((-d,+d), (-d,+d), **kwargs)
        fig.tight_layout()

    # labels
    ax = fig.add_subplot(111, frameon=False)
    ax.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    ax.set_xlabel('observation time [min]')
    ax.set_ylabel('central brightness [arbitrary units]')

    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig


def plot_scale_bl(scale_bls, outname, data, var=None, ylim=None):

    """
    Plot the amplitude scaling factor as a function of time for each baseline 
    
    Parameters:
    -----------
    scale_bls (list)   : ift fields containing the scaling factor for all baselines
    outname (string)   : file name to save plot as .png
    data (dictionary)  : data dictionary as returned by src.data.read_data
    var (list, opt.)   : same format as scales_bl, contains the standard deviation to be plotted as shaded band
    ylim (tuple, opt.) : cnstain range of the y-axis

    Returns: the figure object
    -------
    """
    set_style('plotstyle')
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

    # detect large gaps in the time array
    tdat = data['tim']
    dtime = tdat[1:]-tdat[:-1]
    breakes = np.argwhere(dtime > 60.).flatten()
    nax = len(breakes) + 1
    limits = np.zeros( [nax,2])
    for ib in range(nax-1):
        limits[ib,1]   = tdat[breakes[ib]] + 5.0
        limits[ib+1,0] = tdat[breakes[ib]+1] -5.0
    limits[-1,1] = tdat[-1] + 5.0
    limits[0,0] = -5.0

    if np.any(var!=None):
        assert scale_bls.shape==var.shape, "Missmatch between means and variances passed to plot_scale_bl"
    fig, ax = plt.subplots(1, nax, sharey=True)
    ax = np.array([ax,]).flatten()
    #ax = np.array([ax,]).reshape(npol, nax)

    for ia in range(nax):
        for ibl in range(len(scale_bls)):
            if np.any(var!=None):
                ax[ia].fill_between(tdat, (scale_bls[ibl].val-var[ibl].val), (scale_bls[ibl].val+var[ibl].val),\
                                           color=colors[ibl], alpha=.3, linewidth=0.0)
                ax[ia].plot(tdat, scale_bls[ibl].val, label='bl-{}-{}'.format(*data['baselines'][ibl]), c=colors[ibl])
            [ax[ia].axvline(tt, lw=.5, linestyle='--') for tt in data['tim']]

    ax[-1].legend(ncol=2)

    # multi-plot formatting
    if nax>1:
        d = .015
        for ia, aa in enumerate(ax[ip]):
            aa.set_xlim(*limits[ia])
            kwargs = dict(transform=aa.transAxes, color='k', clip_on=False)
            if ia<nax-1:
                aa.spines['right'].set_visible(False)
                aa.yaxis.tick_left()
                aa.plot((1-d,1+d), (-d,+d), **kwargs)
                aa.plot((1-d,1+d),(1-d,1+d), **kwargs)
                if ia>0:
                    aa.spines['left'].set_visible(False)
                    aa.yaxis.tick_right()
                    aa.plot((-d,+d), (1-d,1+d), **kwargs)
                    aa.plot((-d,+d), (-d,+d), **kwargs)
    fig.tight_layout()

    # labels
    ax = fig.add_subplot(111, frameon=False)
    ax.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    ax.set_xlabel('observation time [min]')
    ax.set_ylabel('amplitude scaling')

    if outname != None:
        fig.savefig(outname)
    if ylim!=None:
        ax.set_ylim(ylim)
    plt.close()
    return fig

    


    
def plot_data_comparison(model, data, data_selection, outname=None, ylim_phi=None, ylim_rho=None, ylim_t3p=None):
    """
    Overplot the data used in the inference and the current model estimation.

    Parameters:
    -----------
    model (ift.MultiField) : model prediction to be plotted
    data (dict)            : data dictionary as returned by data.read_data
    data_selection (array) : data used in the imaging, can contain `phase`, `amplitude`, and `closure`
    outname (string, opt.) : file name to save plot as png
    ylim_phi (tuple, opt.) : constrain y-axis of the visibility phase plot
    ylim_rho (tuple, opt.) : constrain y-axis of the visbility amplitude plot
    ylim_t3p (tuple, opt.) : constrain y-axis of the closure phase plot

    Returns:
    --------
    fig (plt.figure) : the figure object

    """

    set_style('plotstyle')
    fs      = plt.rcParams['figure.figsize']
    colors  = plt.rcParams['axes.prop_cycle'].by_key()['color']
    pltargs = {'lw':.3, 'marker':'o', 'markersize':1}

    ncol = len(data_selection)
    fig, ax = plt.subplots(data['nframe'], ncol, sharex='col', sharey='col', figsize=(0.5*ncol*fs[0], .3*data['nframe']*fs[1]))

    if ncol==1:
        ax = ax[:,np.newaxis]

    # get uv-coordinates
    uv_vis, uv_t3p = get_uv_coordinates(data)

    # if data is masked plot only the model
    mask_vis = [_data_mask(data, ibl, False) for ibl in range(data['nbl'])]
    mask_vis = [mm(ift.full(mm.domain, 1.)).val[0] for mm in mask_vis]
    mask_t3p = [_closure_mask(data, icl) for icl in range(data['ncl'])]
    mask_t3p = [mm[0](ift.full(mm[0].domain,1.)).val for mm in mask_t3p ]

    icol = 0
    # 1. PHASES
    ###########
    if 'phase' in data_selection:

        ax[0, icol].set_title('visibility phase')
        if ylim_phi==None:
            ylim_phi = [-180., 180.]
            
        for ibl in range(data['nbl']):
            
            bl_name = 'bl-{:d}-{:d}'.format(*data['baselines'][ibl])
            vis_bl  = model[bl_name].val[0]
            phaseM  = np.angle(vis_bl, deg=True)
            phaseD  = data['phi'][:,ibl,:,:].copy()*180./np.pi
            phaseE  = data['spi'][:,ibl,:,:].copy()*180./np.pi

            phaseD[mask_vis[ibl]==0] = np.nan
            for ip in range(data['npol']):
                for iff in range(data['nframe']):
                    ax[iff, icol].errorbar(uv_vis[iff,ibl], phaseD[iff,ip,:], yerr=phaseE[iff,ip,:],\
                                           **pltargs, c=lighten_color(colors[ibl]))
                    ax[iff, icol].plot(uv_vis[iff,ibl], phaseM[iff,ip,:], **pltargs, c=colors[ibl])
                    ax[iff, icol].set_ylim(ylim_phi)
        icol += 1

    # 2. AMPLITUDES
    ###############
    if 'amplitude' in data_selection:

        ax[0,icol].set_title('visibility amplitude')
        if ylim_rho==None:
            ylim_rho = [0., 1.]
            
        for ibl in range(data['nbl']):
            bl_name = 'bl-{:d}-{:d}'.format(*data['baselines'][ibl])
            vis_bl  = model[bl_name].val[0]
            ampM    = np.abs(vis_bl)
            ampD    = data['rho'].copy()[:,ibl,:,:]
            ampE    = data['sro'].copy()[:,ibl,:,:]

            ampD[mask_vis[ibl]==0] = np.nan
            for ip in range(data['npol']):
                for iff in range(data['nframe']):
                    ax[iff,icol].errorbar(uv_vis[iff,ibl], ampD[iff,ip,:], yerr=ampE[iff,ip,:],\
                                          **pltargs, c=lighten_color(colors[ibl]) )
                    ax[iff,icol].plot(uv_vis[iff,ibl], ampM[iff,ip,:], **pltargs, c=colors[ibl])
                    ax[iff,icol].set_ylim(ylim_rho)
        icol += 1

    # 3. CLOSURES
    #############
    if 'closure' in data_selection:

        ax[0,icol].set_title('closure phase')
        if ylim_t3p==None:
            ylim_t3p = [-180., 180.]
        for icl, ccl in enumerate(data['closures']):

            bl_names = ['bl-{:d}-{:d}'.format(*data['baselines'][ibl]) for ibl in ccl ]
            csets = ift.ContractionOperator(model[bl_names[0]].domain, spaces=0)
            
            # in the model also show masked data -> this requires to get new masks
            # see likelihood._closure_mask
            masks = []
            for ibl, bl in enumerate(ccl):
                mask = np.full(csets.domain.shape, 0)
                idx_set = np.argwhere(np.all(data['closures'][np.any(data['closures']==bl, axis=-1)] ==ccl, axis=-1))[0,0] + 1
                mask[idx_set,:,:,:] = 1
                masks.append(ift.makeOp(ift.makeField(model[bl_names[0]].domain, mask)))

            cl_op =   (csets @ masks[0] @ ift.FieldAdapter(masks[0].domain, bl_names[0]) ) \
                    * (csets @ masks[1] @ ift.FieldAdapter(masks[1].domain, bl_names[1]) ) \
                    * (csets @ masks[2] @ ift.FieldAdapter(masks[2].domain, bl_names[2]).conjugate() )

            t3pM = np.angle(cl_op.force(model).val, deg=True)
            t3pD = data['t3p'].copy()[:,icl,:,:]*180./np.pi
            t3pE = data['t3e'].copy()[:,icl,:,:]*180./np.pi

            idx_set = np.argwhere(np.all(data['closures'][np.any(data['closures']==ccl[0], axis=-1)] ==ccl, axis=-1))[0,0] + 1
            t3pD[mask_t3p[icl][idx_set]==0] = np.nan
            for ip in range(data['npol']):
                for iff in range(data['nframe']):
                    ax[iff,icol].errorbar(uv_t3p[iff,icl], t3pD[iff,ip,:], yerr=t3pE[iff,ip,:],\
                                          **pltargs, c=lighten_color(colors[icl+data['nbl']]))
                    ax[iff,icol].plot(uv_t3p[iff,icl], t3pM[iff,ip,:], **pltargs, c=colors[icl+data['nbl']])
                    ax[iff,icol].set_ylim(ylim_t3p)
        icol += 1

    # x-axis label:
    for ii in range(icol):
        ax[-1, ii].set_xlabel('spatial frequency [1/arcsec]')

    fig.tight_layout()
    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig

def plot_aberration_op(aberration_maps, oroot, fov):

    """
    Plot phase- and amplitude maps.

    Parameters:
    ----------
    aberration_maps (np.array) : one complex 2D map per telescope
    oroot (strin) : filename root to save the maps to
    fov (float)   : extend of the maps

    Returns: a figure object for phase-  and one for amplitude-maps.
    -------
    """

    fs = plt.rcParams['figure.figsize']
    fig1, ax1 = plt.subplots(2,2, sharex=True, sharey=True, figsize=(fs[0], fs[0]) )
    fig2, ax2 = plt.subplots(2,2, sharex=True, sharey=True, figsize=(fs[0], fs[0]) )
    ax1 = ax1.flatten()
    ax2 = ax2.flatten()

    pltargsP = {'origin':'lower', 'cmap':'twilight_shifted', 'extent': [fov/2, -fov/2, -fov/2, fov/2]}#,\
            #'levels':np.linspace(-180, 180, 19, endpoint=True)}
    pltargsA = {'origin':'lower', 'vmin':0, 'vmax':1, 'extent': [fov/2, -fov/2, -fov/2, fov/2]}#,\
             #   'levels':np.linspace(0., 1., 15, endpoint=True)}

    for io, oo in enumerate(aberration_maps):
        img = np.swapaxes(oo, 0, 1)
        img = np.flip(img, axis=1)

        try:
            imP = ax1[io].imshow(np.angle(img, deg=True), **pltargsP)
        except:
            pltargsP['cmap']='viridis'
            imP = ax1[io].imshow(np.angle(img, deg=True), **pltargsP)
        imA = ax2[io].imshow(np.abs(img), **pltargsA)
        ax1[io].set_aspect('equal')
        ax2[io].set_aspect('equal')

    fig1.tight_layout()
    fig2.tight_layout()

    if oroot != None:
        fig1.savefig(oroot+'_phasemaps.png')
        fig2.savefig(oroot+'_amplitudemaps.png')
    plt.close()
    return fig1, fig2
        

def plot_uv_coverage(data, outname=None):

    """
    Plot uv-covaerage of a specific data set.

    Parameters:
    -----------
    data (dict) : data dictionary as returned by data.read_data
    outbame (string, opt) : weather and where to save the plot in png format

    Returns: the figure object
    -------
    """

    set_style('plotstyle')
    fs      = plt.rcParams['figure.figsize']
    colors  = plt.rcParams['axes.prop_cycle'].by_key()['color']
    pltargs = {'marker':'o', 's':1}

    # get uv-coordinates
    uco     = np.full((data['nframe'], data['nbl'], data['nlam']), np.nan)
    vco     = np.full((data['nframe'], data['nbl'], data['nlam']), np.nan)
    for bl in range(data['nbl']):
        uco[:,bl,:] = np.outer(data['uco'][:,bl], 1./data['lam'])*MAS2RAD
        vco[:,bl,:] = np.outer(data['vco'][:,bl], 1./data['lam'])*MAS2RAD

    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    for ibl in range(data['nbl']):
        for iff in range(data['nframe']):
            #print(iff/data['nframe']+0.2)
            ax.scatter(uco[iff,ibl,:], vco[iff,ibl], **pltargs, c=[lighten_color(colors[ibl], iff/data['nframe']+0.3)])
    ax.set_xlabel('u-coordinate [1/arcsec]')
    ax.set_ylabel('v-coordinate [1/arcsec]')
    labels = ['bl \#{}, tel {} - {}'.format(ibl, *data['baselines'][ibl]) for ibl in range(data['nbl'])]
    hadels = [Line2D([0],[0], linestyle='None', marker='o', c=colors[ibl]) for ibl in range(data['nbl'])]
    ax.legend(hadels, labels)
    ax.text(0.1, 0.03, data['obs_date']+'\n{:d} exposures'.format(data['nframe']), transform=ax.transAxes, fontsize='small')
    fig.tight_layout()

    if outname!=None:
        fig.savefig(outname)
    plt.close()
    return fig


from astropy.io import fits
def save_fits(image, ofile):

    """
    Save images to fits file.
    Note: because of displaying conventions, we swap the RA axis to run from positive to negative.

    Parameters:
    -----------
    image (ift.Field) : the image which should be saved
    ofile (string) : the name of the output file

    Returns: vois
    -------

    """

    # reshape the image such that NORTH->UP and EAST->left
    image2 = np.swapaxes(image.val, 0, 1)
    image2 = np.flip(image2, axis=1)

    outloc = ofile + '.fits'
    hdu    = fits.PrimaryHDU()
    hdl    = fits.HDUList([hdu])
    image  = fits.ImageHDU(data=image2, do_not_scale_image_data=True)
    hdl.append(image)
    hdl.writeto(outloc, overwrite=True)

def save_position(pos, ofile, samples=None):
    """
    Save current position of the inference (useful to restart a run and for analysis).
    
    Parameters:
    -----------

    pos (ift.Multifield) : position to be saved
    ofile (string) : file name for output
    samples (list, opt.) : also save corresponding samples

    Returns: vois
    -------
    """
    dict_to_save = {'position':pos}
    if samples!=None:
        dict_to_save['samples'] = samples
    np.save(ofile, dict_to_save)

    

