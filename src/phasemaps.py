# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
from astropy.io import fits
import nifty7 as ift
from scipy.interpolate import griddata
from astropy.convolution import Gaussian2DKernel
from scipy import signal
from functools import reduce
from operator import add
import os
import re
import time

def get_aberration_op(domain, data, file_name, idx_bl, dalpha, lam0=2.2e-6, clip_low=None, kernel_stddev=10.):

    """
    Wrapper function to obtain the phase- and amplitude-maps per baseline.
    For details on the phasemaps see arXiv:2101.12098.

    Parameters:
    -----------
    domain (ift.Domain) : domain of the maps, i.e. 2D ift.RGSpace of same size as the image.
    data (dictionary) : data dictionary as returned by src.data.read_data
    idx_bl (int) : baseline querried
    dalpha (float) : grid-spacing in the input file in units of mas
    lam0 (float, opt.) : wavelength for which the input file was created
    clip_low (float, opt.) : clip the amplitud emaps at some minimum value
    kernel_stddev (float) : size of the cernel that accounts for turbulen smoothing.

    Returns:
    --------
    aberrations (np.array) : aberration maps in the format (nlam, ntel, npix, npix)
    """

    if file_name[-5:] == '.fits':
        aberrations = _aberration_op_fromdata(domain, data, file_name, idx_bl, dalpha, lam0)

    else:
        print('ERROR: cannot parse the phasemap parametrization. File name must end in .fits or .npy')
        exit()

    if clip_low!=None:
        for il in range(data['nlam']):
            for it in range(2):
                lim = clip_low*np.abs(aberrations[il,it]).max()
                aberrations[il,it] = np.clip(np.abs(aberrations[il,it]), lim, None)*np.exp(1j*np.angle(aberrations[il,it]))
    return aberrations

def get_aberration_catalogue(file_name, data, idx_bl):
    """
    Search the phasemaps available for a particular observation date and returne their parameters.

    Parameters:
    -----------
    file_name (string) : name of the file with the phasemap coefficients.
    data (dict)        : data dictionary as returned by read_data
    idx_bl (int)       : baseline index

    Returns:
    --------
    pm_param (list)    : [spix, npix] of the phasemaps which are available
    """
    # get the type of parametrization considered
    # fits -> Frank-style phase maps
    # npy  -> coefficient-based phase maps
    if file_name[-5:]=='.fits':
        n_tail = -5
    elif file_name[-4:]=='.npy':
        n_tail = -4
    else:
        print('ERROR: cannot parse the phasemap parametrization. Fiel must end in .fits or .npy')
        exit()

    # get the prefix which indicates from which parameters the phasempas where created
    file_prefix = file_name.split('/')[-1][:n_tail]

    # get the location to search for maps
    name = reduce(add, [sl+'/' for sl in file_name.split('/')[:-1]])
    name = name + data['obs_date'] + '/'

    # check if this location exists
    if not os.path.exists(name):
        return []

    # loop over all files ...
    # these now all end in .npy
    param = []
    for fn in os.listdir(name):
        
        # check if it has been created from the right parameter set
        if fn.startswith(file_prefix):

            # check if the # of channels and the basline index match
            ptmp = re.findall('\d*\.?\d+', fn[len(file_prefix):-4])
            if int(ptmp[2]) == data['nlam'] and int(ptmp[3])==idx_bl:
                param.append( [float(ptmp[0]), int(ptmp[1])] )

    return np.array(param)


def _pm_load_from_file(file_name, idx_tel):   
    with fits.open(file_name) as ff:
        pm1 = ff[2].data[idx_tel[0]]
        am1 = ff[1].data[idx_tel[0]]
        pm2 = ff[2].data[idx_tel[1]]
        am2 = ff[1].data[idx_tel[1]]
    cm1 = am1*np.exp(1j*pm1*np.pi/180.)
    cm2 = am2*np.exp(1j*pm2*np.pi/180.)
    return cm1, cm2

def _aberration_op_fromdata(domain, data, file_name, idx_bl, dalpha, lam0):

    """
    Read in phasemap from data, apply fiber shift and north angle rotation, interploate to grid positions and extraplotae
    in wavelength. 

    If an appropraite map is found in `calibration` load and use this.
    """

    spix = domain.distances[0]
    npix = domain.shape[0]
    nlam = len(data['lam'])
    
    # get the right location to search for phasemaps
    name = reduce(add, [sl+'/' for sl in file_name.split('/')[:-1]])
    name = name + data['obs_date'] + '/'
    
    # if the directory does not exist create it
    if not os.path.exists(name):
        os.makedirs(name)
    # get the right file name
    name = name + file_name.split('/')[-1][:-5]
    name = name + '_spix{0:.2f}_npix{1:d}_nlam{2:d}_bl{3:d}.npy'.format(spix, npix, nlam, idx_bl)

    # check the north angle and print a warning if not defined
    if np.any(np.isnan(data['north_angle'])):
        print('WARNING: some north angle is not defined, using zero instead.')
    try:
        # see if appropriate maps already exist
        aberrations = np.load(name, allow_pickle=True)
        #print('aberration-map loaded from file '+name)
        
    except:
        idx_tel = data['baselines'][idx_bl]
        map1, map2 = _pm_load_from_file(file_name, idx_tel)
        mm1  = np.logical_not(np.isnan(map1))
        mm2  = np.logical_not(np.isnan(map2))
        assert map1.shape==map2.shape, "ERROR: inconsistent phase maps passed"

        # map size at lam0
        amax   = (map1.shape[0]-1)/2.*dalpha

        # grid of image-coordinates
        fov    = domain.shape[0]//2 * domain.distances[0]
        xy     = np.arange(-fov, fov, domain.distances[0] )
        yi, xi = np.meshgrid(xy, xy)
        
        aberrations = np.empty((nlam,2)+domain.shape, dtype=np.complex)
        for il, ll in enumerate(data['lam']):
        
            # grid of map-coordinates
            xy     = np.linspace(-amax, amax, map1.shape[0], endpoint=True)*ll/lam0
            xm, ym = np.meshgrid(xy, xy)

            # rotation from acquestion-camer-frame to image frame
            angle1 = -data['north_angle'][idx_tel[0]]*np.pi/180.
            angle2 = -data['north_angle'][idx_tel[1]]*np.pi/180.
            if np.any(np.isnan([angle1, angle2])):
                angle1 = 0
                angle2 = 0

            xm1 =  np.cos(angle1)*xm + np.sin(angle1)*ym - np.mean(data['fib'][idx_tel[0],:, 0])
            xm2 =  np.cos(angle2)*xm + np.sin(angle2)*ym - np.mean(data['fib'][idx_tel[1],:, 0])
            ym1 = -np.sin(angle1)*xm + np.cos(angle1)*ym - np.mean(data['fib'][idx_tel[0],:, 1])
            ym2 = -np.sin(angle2)*xm + np.cos(angle2)*ym - np.mean(data['fib'][idx_tel[1], :,1])

            # interpolate maps to image coordinates
            cmap1 = griddata((xm1[mm1], ym1[mm1]), map1[mm1], (xi, yi), method='nearest')
            
            cmap2 = griddata((xm2[mm2], ym2[mm2]), map2[mm2], (xi, yi), method='nearest')
            
            # aberration operator per telescope
            aberrations[il,0] = cmap1
            aberrations[il,1] = cmap2.conj()
        np.save(name, aberrations)

    return aberrations
