# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np 
import nifty7 as ift

from astropy.convolution import Gaussian2DKernel
from scipy import signal
from scipy.stats import invgamma
from scipy.stats import norm

def initial_conditions(domain, ini_type, pos=None, restart_file=None):
    """
    Create initial conditions for the minimization from a choice of pre-defined recipies.

    Parameters:
    -----------
    domain (ift.MultiDomain) : domain on which the initial conditions are created
    ini_type (string)        : how the initial conditions should be assigned, 
                               the following options are available:
                               - `prior-mode`
                               - `prior-sample`
    restart_file (string)     : file to restart the run from, if provided all other parameters will be ignored, 
                                assume that the file ends in .npy and that the last three characters before that are 
                                the iteration number

    Returns:
    --------
    ift.MultiField - initial conditions
    int            - MGVI iteration to start the inference from
    """
    
    if restart_file!=None:
        initial_position = np.load(restart_file, allow_pickle=True).item()['position']
        n_iteration      = int(restart_file[-7:-4])
        return initial_position, n_iteration

    if ini_type=='prior-mode':
        return ift.MultiField.full(domain, 0.), 0

    elif ini_type=='prior-sample':
        return ift.MultiField.from_random(domain), 0

    else:
        print('ERROR: initial conditions type {} is not implemented.'.format(ini_type))

 
