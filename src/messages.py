# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift
import time
from .likelihood import _data_mask, _closure_mask

def message_time_binning(data, tmax, trange):
    print('')
    print('SgrA* time resolution:')
    print('----------------------')
    print('Observation duration: {:.2f} minutes'.format(data['tim'].max()))
    if trange[0]!=None:
        print('Only picking frames > {:.2f} minutes from the first one'.format(trange[0]))
    if trange[1]!=None:
        print('Only picking frames < {:.2f} minutes from the first one'.format(trange[1]))
    print('Reconstruction duration {:.2f} minutes'.format(tmax)) 
    print('----------------------')
    print('')

def message_final_information(ofile, start_time):
    end_time = time.time()
    tdiff = end_time - start_time
    hours, res = divmod(tdiff, 3600)
    minutes, secondt = divmod(res, 60)
    print('')
    print('----------------------')
    print('Imaging took {0:d}:{1:02d} hrs'.format(int(hours), int(minutes)))
    print('Results saved to {}'.format(ofile))
    print('----------------------')


def message_data_selection(data):
    print('')
    print('Data selection:')
    print('----------------------')
    ntot = 0
    for ibl in range(data['nbl']):
        # we only want to find out which visibilities are masked, so use_closure=False is fine here
        mask = _data_mask(data, ibl, False)
        mask = mask(ift.full(mask.domain, 1.)).val
        ntot += np.sum(mask)
        nframe_p1 = np.all(mask[0,:,0,:] == 0, axis=1)
        npoint_p1 = np.sum(mask[0,:,0,:][~nframe_p1] == 0.).astype(int)
        if data['npol']==2:
            nframe_p2 = np.all(mask[0,:,1,:] == 0, axis=1)
            npoint_p2 = np.sum(mask[0,:,1,:][~nframe_p2] == 0.).astype(int)
            message = [ibl+1, np.sum(nframe_p1).astype(int), np.sum(nframe_p2).astype(int), npoint_p1, npoint_p2]
            print('Baseline {0:d}: ({1:d}/{2:d}) frames and ({3:d}/{4:d}) individual data points excluded in (p1/p2).'.format(*message))
        else:
            message = [ibl+1, np.sum(nframe_p1).astype(int), npoint_p1]
            print('Baseline {0:d}: {1:d} frames and {2:d} individual data points excluded.'.format(*message))
    print('\n-> In total {:d} visibilities are available.'.format(int(ntot)))
    print('----------------------')

def message_data_selection_closure(data, data_use_closure):
    if data_use_closure!=True:
        return
    print('')
    print('Closure phase selection:')
    print('--------------------')
    ntot = 0
    for icl, ccl in enumerate(data['closures']):
        mask    = _closure_mask(data, icl)
        mask    = mask[0](ift.full(mask[0].domain, 1.)).val
        idx_set = np.argwhere(np.all(data['closures'][np.any(data['closures']==ccl[0], axis=-1)] ==ccl, axis=-1))[0,0] + 1
        mask    = mask[idx_set]
        ntot += np.sum(mask)
        nframe_p1 = np.all(mask[:,0,:] == 0, axis=1)
        npoint_p1 = np.sum(mask[:,0,:][~nframe_p1] == 0.).astype(int)
        message = [icl+1, np.sum(nframe_p1).astype(int), npoint_p1]
        if data['npol']>1:
            nframe_p2 = np.all(mask[:,1,:] == 0, axis=1)
            npoint_p2 = np.sum(mask[:,1,:][~nframe_p2] == 0.).astype(int)
            message = [icl+1, np.sum(nframe_p1).astype(int), np.sum(nframe_p2).astype(int), npoint_p1, npoint_p2]
            print('Triangle {0:d}: ({1:d}/{2:d}) frames and ({3:d}/{4:d}) individual closure measurements excluded in (p1/p2).'.format(*message))
        else:
            print('Triangle {0:d}: {1:d} frames and {2:d} individual closure measurements excluded.'.format(*message))
    print('\n-> In total {:d} closures are available.'.format(int(ntot)))
    print('----------------------')

    
    
def message_fiber_position(data):
    print('')
    print('Fiber positions during observation')
    print('----------------------')

    for it in range(data['ntel']):
        ra   = np.mean(data['fib'][:,it,0])
        dra  = np.sqrt(np.var(data['fib'][:,it,0]))
        dec  = np.mean(data['fib'][:,it,1])
        ddec =np.sqrt(np.var(data['fib'][:,it,1]))
        print('Fiber {:d}: RA = {:.2f} +- {:.2f} mas, DEC = {:.2f} +- {:.2f} mas'.format(it+1, ra, dra, dec, ddec))
    print('----------------------')
    


def message_progress(nit, sampling_routine, algorithm, constants):

    n_samp, n_it = sampling_routine(nit)
    current_time       = time.ctime()
    if algorithm=='mgvi':
        alg_str = 'MGVI'
    elif algorithm=='geometric':
        alg_str = 'GeometricKL'
    msg                = [alg_str, nit, current_time, n_samp, n_it]
    print('{} iteration {:d} finished at {}: # of samples = {:d}, # of minimization steps = {:d}.'.format(*msg))
    if len(constants)!=0:
        message = 'Constants: '
        for cc in constants:
            message += cc + ', '
        print(message[:-2])
        
    print()
                                                                                                    
