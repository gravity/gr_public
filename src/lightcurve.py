# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift

class FrameSelection(ift.LinearOperator):

    def __init__(self, domain, data):
        assert len(domain.shape)==1, "ERROR: FrameSelection only defined for 1D fields."
        self._domain = ift.DomainTuple.make(domain)
        self._target = ift.DomainTuple.make(ift.UnstructuredDomain(data['nframe']))
        tb_width = self._domain[0].distances[0]
        self._idx = np.array([int(tt/tb_width) for tt in data['tim']])
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        x = x.val

        if mode==self.TIMES:
            return ift.Field(self._target, x[self._idx])
        else:
            res = np.zeros(self._domain.shape)
            res[self._idx] = x
            return ift.Field(self._domain, res)


# -------------------- LEGACY --------------------

def ps_lightcurve_witzel(f):
    """Power spectrum for SgrA* temporal variability as given in WItzel et al. 2018,
    see Eq. (19) and Table 5, case 3
    
    Parameters:
    -----------
    * f (float) : frequency in 1/minutes
    """
    fb1 = 4.11e-3
    fb2 = 0.31
    ga1 = 2.10
    ga2 = 5.8

    res1 = np.ones(len(f))
    with np.errstate(divide='ignore'):
        res2 = (f/fb1)**(-ga1)
        res3 = (fb2/fb1)**(-ga1) * (f/fb2)**(-ga2)

    res1[f>fb1]  = 0.
    res2[f<=fb1] = 0.
    res2[f>=fb2] = 0.
    res3[f<fb2]  = 0.

    return res1 + res2 + res3

def ps_lightcurve_powerlaw(f, slope=-2, fb=4.1e-3):
    """Simple power-law spectrum with a break at small frequencies 

    Parameters:
    -----------
    f (float, array of floats) : frequency in 1/minutes
    slope (float)              : slopw of the power law
    fb (float)              : break frequency (1/minutes)
    """
    return (fb/(f + fb))**(-slope)


#class FrameSelection(ift.LinearOperator):
#    def __init__(self, domain, active_inds, nlam, bw_resolution=1):
#        self._domain = ift.DomainTuple.make(domain)
#        self._nlam   = nlam*int(bw_resolution)
#        self._idx = np.array(active_inds)
#        target = [ift.UnstructuredDomain(len(self._idx)),\
#                  ift.UnstructuredDomain(self._nlam)]
#        self._target = ift.DomainTuple.make(target)
#        self._capability = self.TIMES | self.ADJOINT_TIMES
#
#    def apply(self, x, mode):
#        self._check_input(x, mode)
#        x = x.val
#        if mode == self.TIMES:
#            res = x[self._idx]
#            res = np.repeat(res[:,np.newaxis], self._nlam, axis=1)
#            return ift.Field(self._target, res)
#        else:
#            res = np.zeros(self._domain.shape)
#            #xx  = np.repeat(x[:,np.newaxis], self._nlam, axis=1)
#            res[self._idx] = np.sum(x, axis=1)
#            return ift.Field(self._domain, res)
