# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift


def amplitude_calibration(data, data_use_closure, sigma=0.1, key_base='xi_scale_bl{:d}'):

    """
    Generative model to apply a time- and baseline-dependent calibration factor for the visibility amplitudes.

    Parameters:
    -----------
    data (dictionary)       : the read-in data as returned by src.data.read_data
    data_use_closure (bool) : whether closures are considered in the imaging
    sigma (float, optional) : the standard deviation of the calibration factor (mean=1)
    key_base (string)       : template for name of calibration excitations in the full 
                              excitation mift.MultiField

    Returns:
    --------
    scale_bl (list of ift.Operators) : operators to transform random excitations to an amplitude scaling
    
    """
    # 1. Draw the scaling factor on each baseline
    # --------------------------------------------
    domain = ift.UnstructuredDomain(data['nframe'])
    shift  = ift.Adder(ift.full(domain, 1.))
    width  = ift.ScalingOperator(domain, sigma)

    scale_bl = [shift @ width @ ift.FieldAdapter(domain, key_base.format(ibl)) for ibl in range(data['nbl'])]
    
    return np.array(scale_bl)
