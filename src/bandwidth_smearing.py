# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler


import numpy as np
import nifty7 as ift
from .constants import MAS2RAD

def bwsmearing_uv(data, res, use_closure=False):

    """
    Refine the uv-coverage to be later able to account for bandwidth smearing by averaging neighboring uv points.
    Comment: in the PhasemapResponse we apply the fine-gridding in frequency instead but the point source respones
             uses this function.

    Parameters:
    -----------
    * data (dictionary) : Measurment data as returned by read_data. It should contain:
                          * the entries 'uco' and 'vco' in the format (time-frame x baseline x polarisation x channel) in units of meter
                          * entry 'lam' with length equal to the number of channels in units of meter
                          * an entry 'bwl' with the bandwidth of each channel in units of meter.
    
    * res (int)         : Number of points to sub-divide each channel in.

    * use closure (bool, opt.) : Indicate wather closure phases should be computed as well.

    Returnes:
    ---------
    uco (array) : Refined u-coordinates in the shape (nset x frame x baseline x polarisation x wavelength)
    vco (array) : Refined v-coordinates in the shape (nest x frame x baseline x polarisation x wavelength)
    nset = 1 + # of closure triangulars in which a baseline appears
    """
    res = int(res)
    new_shape = (1 + int(use_closure)*int(np.sum(data['closures']==0)), ) +  data['uco'].shape + (data['npol'],) + (data['nlam']*res,)

    uco = np.full(new_shape, np.nan)
    vco = np.full(new_shape, np.nan)

    stepping, step = np.linspace(-1 ,1 ,res, endpoint=False, retstep=True)
    stepping += step/2
    if res==1:
        stepping=0
    
    for ibl in range(data['nbl']):
        for il, ll in enumerate(data['lam']):
            for ip in range(data['npol']):
                lam = ll + data['bwl'][il]/2.*stepping
                uco[0, :, ibl, ip, res*il:res*(il+1)] = np.outer(data['uco'][:,ibl], 1./lam)*MAS2RAD
                vco[0, :, ibl, ip, res*il:res*(il+1)] = np.outer(data['vco'][:,ibl], 1./lam)*MAS2RAD
                if use_closure==True:
                    mask = (data['closures']==ibl)
                    for iss in range(int(np.sum(data['closures']==0))):
                        uco[iss+1, :, ibl, ip, res*il:res*(il+1)] = np.outer(data['ut3'][:, mask][:,iss], 1./lam)*MAS2RAD
                        vco[iss+1, :, ibl, ip, res*il:res*(il+1)] = np.outer(data['vt3'][:, mask][:,iss], 1./lam)*MAS2RAD
            
    return uco, vco
        

class BWS_Average(ift.LinearOperator):
    """
    Operator which acts on a field of visibilities, fine gridded over each spectral channels, and averages them for each channel. 

    Parameters:
    -----------
    domain (ift.DomainTuple) : domain on which the fine-gridded visibilities are computed, it's shape is assumed to be
                               nframe x npol x bw_resolution*nlam
    res (int) : number of points used to sub-sample each channel
    """

    def __init__(self, domain, res, dtype=np.complex128):

        if not isinstance(domain, ift.DomainTuple):
            raise TypeError
        if not len(domain.shape)==4:
            raise TypeError
        if not domain.shape[-1]%res==0:
            raise TypeError("Domain size and resolution don't match")

        self._domain     = ift.DomainTuple.make(domain)
        self._res        = int(res)
        shape            = self._domain.shape[:-1] + (int(self._domain.shape[-1]/self._res), )
        target           = [ift.UnstructuredDomain(shape[0]),\
                            ift.UnstructuredDomain(shape[1]),\
                            ift.UnstructuredDomain(shape[2]),\
                            ift.UnstructuredDomain(shape[3])]
        self._target     = ift.DomainTuple.make(target)
        self._dtype      = dtype
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        xx = x.val
        if mode == self.TIMES:
            x2 = np.full(self._target.shape, np.nan, dtype=self._dtype)
            for il in range(self._target.shape[-1]):
                x2[:,:,:,il] = np.mean(xx[:,:,:,il*self._res:(il+1)*self._res], axis=-1)
            return ift.makeField(self._target, x2)
        x2 = np.full(self._domain.shape, np.nan, dtype=self._dtype)
        for il in range(self._target.shape[-1]):
            x2[:,:,:,self._res*il:(il+1)*self._res] = np.repeat(xx[:,:,:,il, np.newaxis], self._res, axis=-1)/self._res
        return ift.makeField(self._domain, x2)
    
