# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift
from scipy.stats import invgamma
from scipy.stats import norm

from .bandwidth_smearing import bwsmearing_uv
from .phasemaps import get_aberration_op, get_aberration_catalogue

def point_source_visibility(data, prior_pos, bw_res, pos_keys, image_domain, cmap_file, dalpha, clip_low, use_closure):
    """
    Returns an operator which computes the visibility of a single point source depending on its position.

    V = A_i(\alpha) A_j(\alpha) exp( i\phi_i(\alpha) - i\phi_j(\alpha) ) exp(-2\pi i b\cdot\alpha/\lanbda)

    Note: visibility phases and closures slightly differ in their uv-coverage, we account for that by evaluating their phases
    independently.

    Phase- and amplitude maps are evaluated at the prior-position. Since the maps are very falt on scales of the prior variance,
    this is a good approximation for their value at the inferred source position.

    Parameters:
    -----------
    data (dictionary)  : GRAVITY data as returned by read_data
    prior_pos (tuple)  : approximate source position (RA, DEC) in mas
    bw_res (int)       : sub-gridding of each channel in order to compute the effect of bandwidth smearing
    pos_keys (tuple)   : name of the source's RA and DEC in the input field
    image_domain (ift.Domain) : domain on which the phasemaps are defined
    dalpha (float)     : grid spacing in the phasemaps if read from fits file
    clip_low (float)   : value at which to clip the phasemaps
    use_closure (bool) : indicate weather visibilities for closure phases should also be computed

    Returns:
    --------
    ps_ops (list of ift.Operators) : the response operator for a single point source on each baseline
    injection (list) : fiber injection coefficient as derived from the amplitude maps for each baseline
    """
    
    # input and output domains
    dom_vis = [ift.UnstructuredDomain(1 + int(use_closure)*int(np.sum(data['closures']==0))),\
               ift.UnstructuredDomain(data['nframe']),\
               ift.UnstructuredDomain(int(bw_res)*data['nlam'])]
    dom_vis = ift.DomainTuple.make(dom_vis)
    dom_pos = ift.ContractionOperator(ift.UnstructuredDomain(data['nlam']), spaces=0).target
    
    # refinde uv-coordinates to approximate bandwidth smearing
    uco, vco = bwsmearing_uv(data, int(bw_res), use_closure)

    # operator to extract source positions
    op_ra  = ift.FieldAdapter(dom_pos, pos_keys[0])
    op_dec = ift.FieldAdapter(dom_pos, pos_keys[1])

    # for each set, frame, baseline, channel compute ...
    # exp( -2*pi*i (RA*u + DEC*v) )
    # u and v do not change between polarization states, so just take the first
    expall = ift.ContractionOperator(dom_vis, spaces=(0,1,2)).adjoint
    ps_ops = []
    for ibl in range(data['nbl']):
        opu = ift.makeOp(ift.makeField(dom_vis, uco[:, :, ibl, 0, :]))
        opv = ift.makeOp(ift.makeField(dom_vis, vco[:, :, ibl, 0, :]))
        op  = -1j*2.*np.pi* ((opu @ expall @ op_ra) + (opv @ expall @ op_dec))
        ps_ops.append(op.exp())

    # get the phasemaps contribution in the prixor position
    # here we assume that the prior is much strikter than the smoothness of the phasemaps or their accuracy
    # so small changes in the source position will not affect the aberration-map contributions
    # A_i(\alpha)*A_j(\alpha) exp( i*\phi_i(\alpha) - i*\phi_j(\alpha))
    # we also compute an array of A_i(\alpha), A_j(\alpha) which is needed for the normalization operator
    spix      = image_domain.distances[0]
    npix_s    = image_domain.shape[0] 
    idx_src   = tuple(np.floor(np.array(prior_pos)/spix + npix_s/2).astype(int))

    # check if the source lies within the image FOV
    if idx_src[0]<npix_s and idx_src[1]<npix_s:
        pm_domain = image_domain
    else:
        # search if a suitable map exists. rank maps by smallest pixelization and largest fov
        pm_avail  = get_aberration_catalogue(cmap_file, data, ibl)
        fov_avail = np.array([pp[0]*pp[1]/2. for pp in pm_avail])
        pm_avail  = pm_avail[fov_avail > np.max(prior_pos)]
        if len(pm_avail)>0:
            spix   = np.min(pm_avail[:,0])
            npix_s = np.max(pm_avail[pm_avail[:,0]==spix][:,1])
        # if no suitable map exists, create one
        # maps get verry inaccurate far off-axis anyway, so fix the number of pixels to a reasonable size
        # and adjust the pixel width in the maps, add a mrgin of 15 mas arround the source (this is roughly the
        # size of the smoothing kernel)
        else:
            npix_s = 256
            spix   = 2.*(np.max(prior_pos) + 5.)/npix_s
        pm_domain = ift.RGSpace(2*(npix_s,), distances=2*(spix,))
        idx_src   = tuple(np.floor(np.array(prior_pos)/spix + npix_s/2).astype(int))
        
    prefac    = np.full((data['nbl'], *dom_vis.shape), np.nan, dtype=np.complex)
    injection = np.full([data['nbl'], 2, data['nlam']], np.nan)
    for ibl in range(data['nbl']):
        pms = get_aberration_op(pm_domain, data, cmap_file, ibl, dalpha, clip_low=clip_low)
        for il in range(data['nlam']):
            #for iss in range(1 + int(use_closure)*int(np.sum(data['closures']==0))):
            prefac[ibl, :, :, il*bw_res:(il+1)*bw_res] = pms[il,0][idx_src]*pms[il,1][idx_src]
            injection[ibl,0,:] = np.abs(pms[il,0][idx_src])**2.
            injection[ibl,1,:] = np.abs(pms[il,1][idx_src])**2.

    # finally, for consistency with the original response operator, we need to apply a volume factor
    prefac *= image_domain.scalar_dvol
    
    # generate the complete point source operator for all baselines
    ps_ops = [ift.makeOp(ift.makeField(dom_vis, prefac[ibl])) @ ps_ops[ibl] for ibl in range(data['nbl'])]
    
    return ps_ops, injection
        
    
