# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import ducc0.wgridder as ng
import numpy as np

import nifty7 as ift

from .constants import SPEEDOFLIGHT, MAS2RAD
from .phasemaps import get_aberration_op
    
class PhasemapResponse(ift.LinearOperator):
    """ Interferometric response on one baseline in the presence of static optical aberrations.
    
    Note: due to the averaging procedure over individual DITS and data flagging the uv-coordinates for visibilities and closures do
    not exactly agree. Instead, we use differen uv-coordinates for visibilities and each closure triangle in which the baseline 
    appears. Computing this in the same operator still is faster than applying an individual operator each time.

    Parameters:
    -----------
    domain (ift.Domain)      : input domain 3D RGSpace (image x frequency)
    data (dict)              : data dictionary as returned by read_data
    bw_res (int)             : number of sub-gridding points per wavelength channel used to compute bandwidth smearing
    epsiolon (float)         : gridder accuracy parameter
    file_name (string)       : path to file which defines the phasemaps
    idx_bl (int)             : baseline index
    dalpha (float)           : grid spacing in phasemaps in units of mas
    use_closure (bool, opt.) : use closure phases for imaging
    clip_low (float, opt.)   : lower value where the amplitude maps should be clipped
    phasemap_kernel (float)  : Standard deviation for smoothing the phase maps.

    Returns:
    --------
    visibilities on a tuple of three unstructured domains w/ sizes nv x nframes x nlam
    -> nv = 1 if use_closures==False
    -> nv = 1 + # of trangles in which the baseline appears if use_closure==True
    """
    
    def __init__(self, domain, data, bw_res, epsilon, file_name, idx_bl, dalpha, clip_low=None, use_closure=False,\
                 phasemap_kernel=10.):

        # 1.) define the uv-coordinates
        self._nframe = data['nframe']
        self._nsets  = 1 + int(use_closure)*int(np.sum(data['closures']==idx_bl))
        self._uvw    = np.zeros((self._nframe*self._nsets, 3))

        # uv coordinates for visibilities
        self._uvw[:self._nframe, 0]  = data['uco'][:,idx_bl]
        self._uvw[:self._nframe, 1]  = data['vco'][:,idx_bl]

        # uv coordinates in closures
        if use_closure==True:
            cl_mask = np.array(data['closures'])==idx_bl
            self._uvw[self._nframe:, 0] = data['ut3'][:,cl_mask].flatten(order='F') # = [*ut3[0], *ut3[1]]
            self._uvw[self._nframe:, 1] = data['vt3'][:,cl_mask].flatten(order='F') # = [*vt3[0], *vt3[1]]
        
        # 2.) refined frequencies for bandwidth smearing
        self._bw_res   = int(bw_res)
        self._lams      = data['lam']
        stepping, step = np.linspace(-1 ,1 ,self._bw_res, endpoint=False, retstep=True)
        stepping      += step/2
        if self._bw_res==1:
            stepping=0
        freq = np.zeros((data['nlam'], self._bw_res))
        for il, ll in enumerate(data['lam']):
            lam = ll + data['bwl'][il]/2.*stepping
            freq[il] = SPEEDOFLIGHT/lam*MAS2RAD
        self._freq = freq

        self._eps        = float(epsilon)

        # 3.) operator domain and target
        self._domain     = ift.makeDomain(domain)
        target           = [ift.UnstructuredDomain(self._nsets),\
                            ift.UnstructuredDomain(data['nframe']),\
                            ift.UnstructuredDomain(self._bw_res*data['nlam'])]
        self._target     = ift.DomainTuple.make(target)
        self._capability = self.TIMES | self.ADJOINT_TIMES

        # 4.) aberration correction
        self._phase_screen  = get_aberration_op(self._domain[0], data, file_name, idx_bl, dalpha,\
                                                clip_low=clip_low, kernel_stddev=phasemap_kernel)

    def apply(self, x, mode):
        self._check_input(x, mode)
        dx, dy = self._domain[0].distances
        nx, ny = self._domain[0].shape
        if mode == self.TIMES:
            res = np.full(self._target.shape, np.nan, dtype=np.complex)
            for il, ll in enumerate(self._lams):
                xx = self._phase_screen[il,1] * self._phase_screen[il,0] * x.val[:,:,il]
                resRE = ng.dirty2ms(self._uvw, self._freq[il], np.real(xx), None, dx, dy, 0, 0, self._eps, False)
                resIM = ng.dirty2ms(self._uvw, self._freq[il], np.imag(xx), None, dx, dy, 0, 0, self._eps, False)
                for ii in range(self._nsets):
                    res[ii,:,self._bw_res*il:self._bw_res*(il+1)] = (resRE + 1j*resIM)[ii*self._nframe:(ii+1)*self._nframe, :]
        else:
            res = np.zeros(self._domain.shape)
            for il, ll in enumerate(self._lams):
                xx     = np.concatenate([x.val[ii,:,self._bw_res*il:self._bw_res*(il+1)] for ii in range(self._nsets)])
                resRE  = ng.ms2dirty(self._uvw, self._freq[il],     xx, None, nx, ny, dx, dy, 0, 0, self._eps, False)
                resIM  = ng.ms2dirty(self._uvw, self._freq[il], -1j*xx, None, nx, ny, dx, dy, 0, 0, self._eps, False)
                resALL = self._phase_screen[il,0].conj() * self._phase_screen[il,1].conj() * (resRE+1j*resIM)
                res[:,:,il] = np.real(resALL)
        res *= self._domain[0].scalar_dvol
        return ift.makeField(self._tgt(mode), res)

def get_NormOp_bl(norm_type, data, ibl, dom_skys, cmap_file, kwargs_norm):
    """ Wrapper function to return the desired normalization operator for the specified baseline
    
    Parameters:
    -----------
    norm_type (string) : the type of operator to be constructed, currently only `deterministic` is available
    data (dict)        : data dictionary as returned by read_data
    ibl (int)          : index of the baseline to consider
    dom_sky (list)     : list of domains (one per polarization) on which the operator acts
    cmap_file (string) : location to obtaine phasemaps from
    kwargs_norm (dict) : optional arguments for NormOp_PmLSB

    Returns:
    --------
    norm_ops (list of ift.Operators) : the normalization operators on the requested baseline for each polarization state

    Note:
    -----
    * the normalization operators differ in both polarization states because the SgrA* lightcurve is different
    """

    sky_keys_p1 = ['sky_alpha2', 'variability_p1', 'alpha1', 'alpha2', 'alpha_bkg', 'flux_bkg']
    sky_keys_p2 = ['sky_alpha2', 'variability_p2', 'alpha1', 'alpha2', 'alpha_bkg', 'flux_bkg']    

    norm_ops = []
    if norm_type=='deterministic':
        op = NormOp_PmLSB(dom_skys[0], data, cmap_file, ibl, 1., sky_keys_p1, **kwargs_norm).real
        norm_ops.append(op**(-0.5))
        if data['npol']==2:
            op = NormOp_PmLSB(dom_skys[1], data, cmap_file, ibl, 1., sky_keys_p2, **kwargs_norm).real
            norm_ops.append(op**(-0.5))
    else:
        print('ERROR: normalization operator of type {} is not implemented.'.format(norm_type))
        exit()

    return np.array(norm_ops)

class NormOp_PmLSB(ift.Operator):
    """Normalization Operator taking into account phasemaps and variability of the central source
    
    Parameters:
    -----------
    domain (MultiDomain)  : operators input domain, the first two components will be interpreted as images, 
                            the 3rd is the lightcurve
                            the 4th and 5th components are the spectral indices
    data (dict)           : GRAVITY data structure as returned by read_data
    file_name (string)    : location of phase- and amplitude-maps
    idx_bl (int)          : baseline index (can be 0-5)
    dalpha (float)        : grid size in the phase-/amplitude-maps (units mas)
    keys (list ofstrings) : the keys of components in the following order:
                            [1] image of faint sources
                            [2] variability in the given polarization state (empty qote for static sources)
                            [3] spectral index of the variable component
                            [4] spectral index of the static component
                            [5] spectral index of the background
                            [6] background flux
    injection (array) : list of injection coefficients for both telescopes in the baseline (format = 2 x nlam)
                        which multiply the flux of the lightcurve of the variable source 
    extra_ps_flux_keys (array, optional) : list w/ the names of faint point source fluxes
    extra_ps_injection (array, optional) : fiber injection for all faint sources in the list

    Note:
    -----
    * this operator does not expand in the polarization direction, this has to be done externally, 
      e.g. by an adjoint contraction operator
    
    """
    
    def __init__(self, domain, data, file_name, idx_bl, dalpha, keys, injection,\
                 clip_low=None,\
                 phasemap_kernel=10,\
                 extra_ps_flux_keys=[],\
                 extra_ps_injection=[]):

        # check the input for consistency
        assert isinstance(domain, ift.MultiDomain), "PhasemapNormalization expects a MultiDomain."
        assert len(extra_ps_flux_keys)==len(extra_ps_injection),\
            "Number of flux keys provided for additional point sources does not match number of injections provided."

        # get relevant domains
        self._domain  = ift.makeDomain(domain)
        self._keys    = keys
        target        = [ift.UnstructuredDomain((data['nframe'])),\
                         ift.UnstructuredDomain((data['nlam']))]
        self._target  = ift.DomainTuple.make(target)

        # fiber injection
        # because this term replaces the integral over intensity x photometic lobe we have to weight by the
        # volume factor of the integration
        self._injection = injection*domain[keys[0]][0].scalar_dvol
        self._injection = [ift.makeField(self._target[1], self._injection[itel]) for itel in range(2)]

        # extra static point sources
        self._extra_ps_n = len(extra_ps_flux_keys)
        if self._extra_ps_n > 0:
            self._extra_ps_flux_keys = extra_ps_flux_keys
            self._extra_ps_injection = []
            for ips in range(self._extra_ps_n):
                inj = extra_ps_injection[ips]*domain[keys[0]][0].scalar_dvol
                inj = [ift.makeField(self._target[1], inj[itel]) for itel in range(2)]
                self._extra_ps_injection.append(inj)
        
        # phasemap information
        img_dom       = domain[self._keys[0]][0]
        phase_screen  = get_aberration_op(img_dom, data, file_name, idx_bl, dalpha,\
                                          clip_low=clip_low, kernel_stddev=phasemap_kernel)
        self._ii_lobe = [ift.makeOp(ift.makeField(img_dom, np.real(pp.conj()*pp) )) for pp in phase_screen[:,0] ]
        self._jj_lobe = [ift.makeOp(ift.makeField(img_dom, np.real(pp.conj()*pp) )) for pp in phase_screen[:,1] ]
        self._iB_lobe = SpecialInt(self._ii_lobe)(ift.full(img_dom, 1.))
        self._jB_lobe = SpecialInt(self._jj_lobe)(ift.full(img_dom, 1.))

        # wavelength terms
        lam      = data['lam']
        dll      = data['bwl']/2.
        ll_term_m = (lam - dll)/2.2e-6
        ll_term_p = (lam + dll)/2.2e-6
        ll_norm   = 1./2./dll*2.2e-6
        self._ll_term_m = ift.makeField(target[1], ll_term_m).log()
        self._ll_term_p = ift.makeField(target[1], ll_term_p).log()
        self._ll_norm   = ift.makeField(target[1], ll_norm)
        self._expframe  = ift.ContractionOperator(self._target, spaces=0).adjoint
        self._explam    = ift.ContractionOperator(self._target[1], spaces=0).adjoint
        self._explam_2  = ift.ContractionOperator(self._target, spaces=1).adjoint

    def apply(self, x):
        
        # assemble the wavelength-terms
        alpha1 = self._explam(x[self._keys[2]])
        alpha2 = self._explam(x[self._keys[3]])
        alphaB = self._explam(x[self._keys[4]])
        ll_term1 = ((-alpha1*self._ll_term_m).exp() - (-alpha1*self._ll_term_p).exp()) *self._ll_norm * alpha1**(-1.)
        ll_term2 = ((-alpha2*self._ll_term_m).exp() - (-alpha2*self._ll_term_p).exp()) *self._ll_norm * alpha2**(-1.)
        ll_termB = ((-alphaB*self._ll_term_m).exp() - (-alphaB*self._ll_term_p).exp()) *self._ll_norm * alphaB**(-1.)

        # integrate over intensity x photometric lobe
        i1 = self._injection[0]
        j1 = self._injection[1]
        sky2 = x[self._keys[0]]
        i2 = SpecialInt(self._ii_lobe)(sky2)
        j2 = SpecialInt(self._jj_lobe)(sky2)

        # if there are additional point sources, add them to sky2
        for ips in range(self._extra_ps_n):
            ps_flux = x[self._extra_ps_flux_keys[ips]]
            i2 = i2 + self._extra_ps_injection[ips][0] * self._explam(ps_flux)
            j2 = j2 + self._extra_ps_injection[ips][1] * self._explam(ps_flux)
        

        # multiply by wavelength terms
        i1 = self._expframe(ll_term1*i1)
        i2 = self._expframe(ll_term2*i2)
        j1 = self._expframe(ll_term1*j1)
        j2 = self._expframe(ll_term2*j2)

        # get the background term
        bkg = self._explam(x[self._keys[5]])
        ib  = self._expframe(bkg*ll_termB *self._iB_lobe)
        jb  = self._expframe(bkg*ll_termB *self._jB_lobe)

        # apply source variability
        if self._keys[1]!='':
            sgrA = x[self._keys[1]]
            i1 = self._explam_2(sgrA)*i1
            j1 = self._explam_2(sgrA)*j1
                
        res = (i1+i2+ib)*(j1+j2+jb)
        return res    
    
class SpecialInt(ift.LinearOperator):
    def __init__(self, lst):
        self._lst    = lst
        self._domain = lst[0].domain
        self._target = ift.DomainTuple.make(ift.UnstructuredDomain(len(self._lst)))
        self._capability = self.TIMES | self.ADJOINT_TIMES

    def apply(self, x, mode):
        self._check_input(x, mode)
        intop = ift.ContractionOperator(self._lst[0].domain, None, 1)
        if mode == self.TIMES:
            res = np.empty(self.target.shape)
            for ii, ll in enumerate(self._lst):
                res[ii] = (intop @ ll)(x).val
        else:
            res = np.zeros(self.domain.shape)
            for ii, ll in enumerate(self._lst):
                res += (intop @ ll).adjoint(ift.Field.scalar(x.val[ii])).val
        return ift.makeField(self._tgt(mode), res)
    
