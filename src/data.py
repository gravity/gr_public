# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as fit
from astropy.io import fits
from astropy.time import Time, TimeDelta
import datetime

def read_data(ifile,
              min_channel=3,
              max_channel=12,
              polarization='P12',
              dtype=None,
              sim_frank=False,
              trange=(None, None),
              insname = 'GRAVITY_SC',
              idx_frame_exclude=[]):

    """
    Read in data from a night-combined fits file.

    Parameters:
    -----------
    ifile (string)   : input file
    min_channel(int) : index of lowest wavelength channel to consider
    max_channel(int) : index of highest wavelength channel to consider
    trange (tuple)   : minimum and maximum time after the first frame to consider in minutes, if None use all available frames
    insname (string) : instument name whose data we want to get, usually `GRAVITY_SC`
                       in combined polarization mode, the appropriate extensions will be appended

    Returns:
    --------
    data_dict (dictionary) : a dictionary with all relevant data categories in the shape time-frame x baseline x polarisation x channel
                             phi = visibility phase (rad)
                             rho = visibility amplitude
                             spi = standard deviation of the visibility phase (rad)
                             sro = standard deviation of the visibility amplitude
                             t3p = closure phase (rad)
                             t3e = standard deviation of the closure phase (rad)
                             uco = u-coordinate (dimensions = frame x baseline, units = m)
                             vco = v-coordinate (dimensions = frame x baseline, units = m)
                             ut3 = u-coordinates for closures (dimensions = frame x closure x 3, units = m)
                             vt3 = v-coordinates for closures (dimensions = frame x closure x 3, units = m)
                             lam = wavelength (meters) (dimensions = channel)
                             tim = time measured from the first frame in minutes (dimensions = time-frame)
                             fib = fiber positions in rad and dec, (dimensions = nframe x ntel x 2)
    Comments:
    ---------
    * Flagged data has to be indicated by a negative standard deviation in the input fits-file 
      and is excluded by the likelihood.
    """

    # check time cuts for consistency
    if trange[0]!=None and trange[1]!=None:
        assert trange[0]<trange[1], "Inconsistent choice of time limits."

    slam        = slice(min_channel, max_channel+1)
    north_angle = np.zeros(4)

    with fits.open(ifile) as ff:
        nlam = len(ff['OI_WAVELENGTH'].data['EFF_WAVE'][slam]) # number of channels
        nbl  = len(ff['OI_VIS'].data['UCOORD'])                # number of baselines
        ntel = len(ff['OI_ARRAY'].data['STA_INDEX'])           # number of telescopes
        ncl  = len(ff['OI_T3'].data['TIME'])                   # number of closures
        nframe = 0
        for tab in ff[1:]:
            nframe += tab.header['EXTNAME']=='OI_VIS'
        polmode = ff[0].header['HIERARCH ESO INS POLA MODE']
        for itel in range(4):
            keyword = 'ESO QC ACQ FIELD{:d} NORTH_ANGLE'.format(itel+1)
            north_angle[itel] = ff[0].header[keyword]
 
    npol = 1
    if polmode=='SPLIT':
        nframe = nframe//2
        if polarization=='P12':
            npol = 2

    if polmode=='SPLIT' and polarization=='P1':
        insname = [insname+'_P1']
    elif polmode=='SPLIT' and polarization=='P2':
        insname = [insname+'_P2']
    elif polmode=='SPLIT' and polarization=='P12':
        insname = [insname+'_P1', insname+'_P2']
    elif polmode=='COMBINED':
        insname = [insname]
    insname=np.array(insname)

    # obtaine visibility and uv coordinates
    # we assume each OI_VIS table containes one time frame
    phi    = np.full((nframe, nbl, npol, nlam), np.nan)
    rho    = np.full((nframe, nbl, npol, nlam), np.nan)
    spi    = np.full((nframe, nbl, npol, nlam), np.nan)
    sro    = np.full((nframe, nbl, npol, nlam), np.nan)
    t3p    = np.full((nframe, ncl, npol, nlam), np.nan)
    t3e    = np.full((nframe, ncl, npol, nlam), np.nan)
    ut3    = np.full((nframe, ncl, 3), np.nan)
    vt3    = np.full((nframe, ncl, 3), np.nan)
    uco    = np.full((nframe, nbl), np.nan)
    vco    = np.full((nframe, nbl), np.nan)
    lam    = np.full(nlam, np.nan)
    bwl    = np.full(nlam, np.nan)
    tim    = np.full(nframe, np.nan)
    fib    = np.full((nframe, ntel, 2), np.nan)

    # keys needed to read out the fiber position
    keys_fiber_ra  = ['HIERARCH ESO QC MET SOBJ DRA{:d}'.format(tt+1) for tt in range(ntel)]
    keys_fiber_dec = ['HIERARCH ESO QC MET SOBJ DDEC{:d}'.format(tt+1) for tt in range(ntel)]

    # get some initial information
    with fits.open(ifile) as ff:
        # wavelength
        lam = ff['OI_WAVELENGTH'].data['EFF_WAVE'][slam]
        bwl = ff['OI_WAVELENGTH'].data['EFF_BAND'][slam]

        # collect the time at which observations where taken
        it = 0
        for tab in ff:
            try:
                if tab.header['EXTNAME']=='OI_VIS' and tab.header['INSNAME']==insname[0]:
                    tim[it] = tab.data['MJD'].mean()
                    it+=1
            except:
                continue
        tim = np.sort(tim)

        # now loop over all tables in the file and select what you need in a time-sorted list
        # here we read all available frames and apply the time cut later on
        for tab in ff[1:]:

            if tab.header['EXTNAME'] in ['OI_ARRAY', 'OI_TARGET', 'OI_WAVELENGTH']:
                continue
            if tab.header['INSNAME'] not in insname:
                continue
            
            # find appropriate polarization and time indices to fill
            idx_pol = 0
            if polmode=='SPLIT' and npol>1:
                idx_pol = np.argwhere(insname==tab.header['INSNAME'])[0,0]
            idx_t = np.argmin(np.abs(tim-tab.data['MJD'].mean()))

            # extract visibilities
            if tab.header['EXTNAME']=='OI_VIS':
                phi[idx_t,:,idx_pol,:] = tab.data['VISPHI'][:,slam]/180.*np.pi
                rho[idx_t,:,idx_pol,:] = tab.data['VISAMP'][:,slam]
                spi[idx_t,:,idx_pol,:] = tab.data['VISPHIERR'][:,slam]/180.*np.pi
                sro[idx_t,:,idx_pol,:] = tab.data['VISAMPERR'][:,slam]
                if idx_pol==0:
                    uco[idx_t,:] = tab.data['UCOORD']
                    vco[idx_t,:] = tab.data['VCOORD']

                # also get the fiber pointing
                fib[idx_t,:,0] = np.array([tab.header[kk] for kk in keys_fiber_ra])
                fib[idx_t,:,1] = np.array([tab.header[kk] for kk in keys_fiber_dec])

            # extract closure quantities
            elif tab.header['EXTNAME']=='OI_T3':
                t3p[idx_t,:,idx_pol,:] = tab.data['T3PHI'][:,slam]/180.*np.pi
                t3e[idx_t,:,idx_pol,:] = tab.data['T3PHIERR'][:,slam]/180.*np.pi
                if idx_pol==0:
                    ut3[idx_t,:,0] = tab.data['U1COORD']
                    ut3[idx_t,:,1] = tab.data['U2COORD']
                    vt3[idx_t,:,0] = tab.data['V1COORD']
                    vt3[idx_t,:,1] = tab.data['V2COORD']

    ut3[:,:,2] = ut3[:,:,0]+ut3[:,:,1]
    vt3[:,:,2] = vt3[:,:,0]+vt3[:,:,1]

    assert ~np.any(np.isnan(phi)), 'Inconsistent visibilities, not all columns filled'
    assert ~np.any(np.isnan(rho)), 'Inconsistent visibilities, not all columns filled'
    assert ~np.any(np.isnan(t3p)), 'Inconsistent closure phases, no all columns filled'
    assert ~np.any(np.isnan(ut3)), 'Inconsistent closure u-coordinates, not all columns filled.'
    assert ~np.any(np.isnan(vt3)), 'Inconsistent closure v-coordinates, not all columns filled.'

    # get the observation date
    # we label a data set accordint to the night in which the observation was taken
    # that means if all data is taken after 12pm we have to shift the date back by a day
    # this avoids confusing two consecutive nights, where the first observed in the 2nd
    # and the other in the 1st half of a night
    tobs = Time(tim.min(), format='mjd').datetime
    if tobs.hour < 12:
        tobs -= datetime.timedelta(days=1)
    tobs = tobs.strftime("%Y-%m-%d")

    tim = (tim - tim.min())*24.*60.

    # exclude specific frames
    fmask = np.full(len(tim), True)
    for idx in idx_frame_exclude:
        fmask[int(idx)] = False

    # now apply the time cuts
    idx_tmin, idx_tmax = None, None
    if trange[0]!=None:
        idx_tmin = np.argwhere(tim[fmask]>trange[0])[0,0]
    if trange[1]!=None:
        idx_tmax = np.argwhere(tim[fmask]>trange[1])[0,0]
    stim = slice(idx_tmin, idx_tmax)

    data_dict = {'phi'    : phi[fmask][stim],
                 'rho'    : rho[fmask][stim],
                 'spi'    : spi[fmask][stim],
                 'sro'    : sro[fmask][stim],
                 't3p'    : t3p[fmask][stim],
                 't3e'    : t3e[fmask][stim],
                 'uco'    : uco[fmask][stim],
                 'vco'    : vco[fmask][stim],
                 'ut3'    : ut3[fmask][stim],
                 'vt3'    : vt3[fmask][stim],
                 'fib'    : fib[fmask][stim],
                 'lam'    : lam,
                 'bwl'    : bwl,
                 'tim'    : tim[fmask][stim],
                 'nbl'    : nbl,
                 'ntel'   : ntel,
                 'ncl'    : ncl,
                 'npol'   : npol,
                 'nlam'   : nlam,
                 'nframe' : len(tim[fmask][stim]),
                 'north_angle'    : north_angle,
                 'baselines'      : [(0,1), (0,2), (0,3), (1,2), (1,3), (2,3)],
                 'closures'       : np.array([[0,3,1], [0,4,2], [1,5,2], [3,5,4]]),
                 'obs_date'       : tobs,
                 #'polmode':polmode
    }
    
    return data_dict
    
