# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift
from functools import reduce
from operator import add

def get_likelihood(data, lh_type, data_selection):
    """
    This is a wrapper function to select and return the correct likelihood.

    Parameters:
    -----------
    data (dict) : the GRAVITY data dictionary as returned by src.data.read_data
    ibl (int)   : index of the baseline considered
    lh_type (string) : form of the likelihood, can be `convex_approx` or 'phase_amplitude'
    data_selection (string) : what type of data to use, can contain `phase`, `amplitude` and `closure`

    Returns:
    --------
    lh (list) : the likelihood for each data type
                for `convex_approx` only one joint likelihood is returned
                otherwise the ordering is amplitude - phase - closure

    """

    # check if closure phases are used in the inference
    use_closure = 'closure' in data_selection
    use_vis = 'phase' in data_selection or 'amplitude' in data_selection

    # create a list of all likelihood contributions to be considered
    lhs = []


    if lh_type=='convex_approx':
        if not ('phase' in data_selection and 'amplitude' in data_selection):
            print('ERROR: your data selection is not compatible with the convex approximation likelihood.'.format(data_selection))
            exit()
        if 'closure' in data_selection:
            print('ERROR: cannot combine lh_type convex_approx with closure phase imaging.')
            exit()
        lhs.append(likelihood_convexapprox(data, use_closure) )

    elif lh_type=='phase_amplitude':
        if 'amplitude' in data_selection:
            tmp_selection = ['amplitude']
            if use_closure: tmp_selection += ['closure']
            lhs.append(likelihood_phase_amplitude(data, tmp_selection))
        if 'phase' in data_selection:
            tmp_selection = ['phase']
            if use_closure: tmp_selection += ['closure']
            lhs.append(likelihood_phase_amplitude(data, tmp_selection))
        if use_closure:
            if 'phase' in data_selection:
                print('WARNING: imaging phases and closures at the same time is inconsistent from an information-theory viewpoint.')
                print(' ... I am going to do it but I told you!\n')
            lhs.append(likelihood_t3p(data))

    else:
        print('ERROR: requested likelihood type {} not implemented.'.format(lh_type))
        exit()

    return lhs


def inverse_covariance_convexapprox(data, ibl, eps):

    """
    Returns two ift.LinearOperators which can be passes as inverse covriances to ift.GaussianEnergy for the read and
    imaginary parts of the data respectively.

    Covariances are computed in the quadratic, convex approximation where the likelihood reads (c.f. arXiv 1708.08390)

    - ln P(d|x) = \\sum_k \\Re(\\epsilon_k e^{-i\\phi_k})^2/var{\\rho_k} + \\Im(\\epsilon e^{-i\\phi_k})^2/(\\rho_k var{\\phi_k}
    w/ \\epsilon e^{i\\phi} = d - R(x)

    Parameters:
    -----------
    data (dictionary)  : data as returned from src.data.read_data
    ibl (int)          : index of the baseline to consider
    eps (float)        : accuracy parameter to avoid division by zero

    Returns:
    --------
    cov_real (ift.DiagonalOperator) : inverse covariance for the real part of the data
    cov_imag (ift.DiagonalOperator) : inverse covariance for the imaginary part of the data

    """
    dom = [ift.UnstructuredDomain(data['nframe']),\
           ift.UnstructuredDomain(data['npol']),\
           ift.UnstructuredDomain(data['nlam'])]
    dom = ift.DomainTuple.make(dom)

    cov_real = (1./(data['sro'][:,ibl,:,:]**2+eps))
    cov_real = ift.makeField(dom, cov_real)
    cov_real = ift.DiagonalOperator(cov_real)
    
    cov_imag = (data['rho'][:,ibl,:,:]*data['spi'][:,ibl,:,:])**2
    cov_imag = 1./(cov_imag + eps)
    cov_imag = ift.makeField(dom, cov_imag)
    cov_imag = ift.DiagonalOperator(cov_imag)

    return cov_real, cov_imag

def _data_mask(data, ibl, use_closure):
    """
    Create mask for visibility phases and amplitudes.

    Parameters:
    -----------
    data (dictionary)  : data as returned by src.data.read_data
    ibl (int)          : index of the baseline to consider
    use_closure (bool) : wether closures are considered in the imaging

    Returns:
    mask (ift.DiagnoalOperator) : the mask operator

    Note:
    -----
    * Flagged data is indicated by a standard deviation smaller than zero in the data dictionary.

    """
    dom  = [ift.UnstructuredDomain(1 + int(use_closure)*int(np.sum(data['closures']==0))),\
            ift.UnstructuredDomain(data['nframe']),\
            ift.UnstructuredDomain(data['npol']),\
            ift.UnstructuredDomain(data['nlam'])]
    dom  = ift.DomainTuple.make(dom)
    mask = np.full(dom.shape, 1)

    # Introduce reasons to exclude data one by one
    ###
    # 1. only use visibilty baselines (not those for closures if present)
    mask[1:, :, :, :] = 0
    
    # 2. visibility amplitudes must be smaller than one
    #    if not true exclude the full baseline
    criterium = np.any(data['rho'][:,ibl,:,:] > 1., axis=-1)
    #mask[:, criterium] = 0

    # 3. visibility amplitudes must be larger than zero
    #    if not true exclude only the data point
    criterium = data['rho'][:,ibl,:,:] < 0.
    mask[:, criterium] = 0
    
    # 4. standard deviations must be greater than zero
    #    if not true exclude the data point
    criterium = data['sro'][:,ibl,:,:] <= 0.
    mask[:, criterium] = 0
    criterium = data['spi'][:,ibl,:,:] <= 0.
    mask[:, criterium] = 0

    mask = ift.makeField(dom, mask)
    return ift.makeOp(mask)

def _closure_mask(data, icl):
    """
    Creat masks operators for closure phases. This is a list of three masks, one for each baseline invloved in 
    the triangle.

    Parameters:
    -----------
    data (dictionary)  : data as returned by src.data.read_data
    icl (int)          : index of the triangle to consider
    """
    # here we can assume that data_use_closure=True
    dom = [ift.UnstructuredDomain(1+int(np.sum(data['closures']==0))),\
           ift.UnstructuredDomain(data['nframe']),\
           ift.UnstructuredDomain(data['npol']),\
           ift.UnstructuredDomain(data['nlam'])]
    dom = ift.DomainTuple.make(dom)

    cl_masks = []
    
    # loop over all baselines invloved in the triangle
    for ibl, bl in enumerate(data['closures'][icl]):

        # set everything to zero initially
        mask = np.full(dom.shape, 0)

        # now select the right index for the given triangle
        # we need to increment by one because the 0th entry is for visibility phases
        idx_set = np.argwhere(np.all(data['closures'][np.any(data['closures']==bl, axis=-1)]\
                                     == data['closures'][icl], axis=-1))[0,0]
        idx_set = idx_set + 1
        mask[idx_set,:,:,:] = 1

        # now in the active set exclude flagged data
        criterium = data['t3e'][:,icl,:,:] <= 0.
        mask[idx_set,criterium] = 0

        # finally transform into an operator
        mask = ift.makeOp(ift.makeField(dom, mask))
        cl_masks.append(mask)
        
    return cl_masks
          

def likelihood_convexapprox(data, use_closure, eps=1.e-8):

    """
    Returns the likelihood of visibilities measured at one baselibe in the quadratic, convex approximation, i.e.

    - ln P(d|x) = \\sum_k \\Re(\\epsilon_k e^{-i\\phi_k})^2/var{\\rho_k} + \\Im(\\epsilon e^{-i\\phi_k})^2/(\\rho_k var{\\phi_k}
    w/ \\epsilon e^{i\\phi} = d - R(x)

    Parameters:
    -----------
    data (dictionary)   : data as returned fro .data.read_data
    ibl (int)           : index of the baseline to consider
    eps (float)         : small value > 0 to avoid division by zero in the covariance matrix

    Returns:
    --------
    likelihood (ift.GaussianEnergy)
    """
    nsets = 1 + int(use_closure)*int(np.sum(data['closures']==0))
    dom = [ift.UnstructuredDomain(data['nframe']),\
           ift.UnstructuredDomain(data['npol']),\
           ift.UnstructuredDomain(data['nlam'])]
    dom = ift.DomainTuple.make(dom)

    lhs_real = []
    lhs_imag = []
    for ibl in range(data['nbl']):

        mask  = _data_mask(data, ibl, use_closure)
        csets = ift.ContractionOperator(mask.domain, spaces=0)

        # extract the right visibility
        bl_name = 'bl-{:d}-{:d}'.format(*data['baselines'][ibl])
        bl_op   = ift.FieldAdapter(mask.domain, bl_name)


        # here we only use the 1st component of nsets and collapse the model prediction after applying the mask
        data_vis = data['rho'][:,ibl,:,:]*np.exp(1j*data['phi'][:,ibl,:,:])
        data_vis = ift.makeField(dom, data_vis)

        cov_real, cov_imag = inverse_covariance_convexapprox(data, ibl, eps)

        lhs_real.append(ift.GaussianEnergy(mean=data_vis.real, inverse_covariance=cov_real) @ csets @ mask.real @ bl_op)
        lhs_imag.append(ift.GaussianEnergy(mean=data_vis.imag, inverse_covariance=cov_imag) @ csets @ mask.imag @ bl_op)

    return reduce(add, lhs_real) + reduce(add, lhs_imag)

def likelihood_phase_amplitude(data, data_selection, eps=1.e-8):
    """
    Create a likelihood operator that assumes independent phases and amplitudes.
    It acts on a MultiField with one entry per baseline. The naming convention in the MultiField is bl-{idx_tel1}-{idx_tel2}.
    For the usual GRAVITY observations the telescope indices run from 0 to 3.

    Parameters:
    -----------
    data (dict)        : data instance as returned by read_data
    use_closure (bool) : indicate weather the visibility field also contains visibilities for closure phases 
                         (which need to be ignored here)
    eps (double)       : fudge-factor to exclude division by zero (irrelevant in the end because these data points 
                         are flagged anyway)

    Return:
    ------
    sum of ift.GaussianEnegry operators - the combined likelihood of all baselines
    """
    use_closure = 'closure' in data_selection
    nsets = 1 + int(use_closure)*int(np.sum(data['closures']==0))

    # loop over baselines
    lhs_phi = []
    lhs_rho = []
    for ibl in range(data['nbl']):
            
        # mask data w/ zero error bars or visibility amplitudes > 1
        mask = _data_mask(data, ibl, use_closure)
        expset = ift.ContractionOperator(mask.target, spaces=0).adjoint

        # extract the right visibility
        bl_name = 'bl-{:d}-{:d}'.format(*data['baselines'][ibl])
        bl_op   = ift.FieldAdapter(mask.domain, bl_name)
        
        # data
        data_rho = np.repeat(data['rho'][np.newaxis,:,ibl,:,:], nsets, axis=0)
        data_phi = np.repeat(np.exp(1j*data['phi'])[np.newaxis,:,ibl,:,:], nsets, axis=0)
        data_rho = mask(ift.makeField(mask.domain, data_rho ))
        data_phi = mask(ift.makeField(mask.domain, data_phi ))

        # inverse covariance matrices
        cov_phi = np.repeat(data['spi'][np.newaxis,:,ibl,:,:], nsets, axis=0)
        cov_rho = np.repeat(data['sro'][np.newaxis,:,ibl,:,:], nsets, axis=0)
        cov_phi = ift.DiagonalOperator(mask(ift.makeField(mask.domain, 1./(cov_phi**2.+eps))))
        cov_rho = ift.DiagonalOperator(mask(ift.makeField(mask.domain, 1./(cov_rho**2.+eps))))

        # operator to convert to the unit circle and amplitude
        phi_op = ToUnitCircle(mask.domain)
        rho_op = ToAmplitude(mask.domain)

        # likelihoods of the individual components
        lhs_phi.append(ift.GaussianEnergy(mean=data_phi, inverse_covariance=cov_phi) @ mask @ phi_op @ bl_op)
        lhs_rho.append(ift.GaussianEnergy(mean=data_rho, inverse_covariance=cov_rho) @ mask @ rho_op @ bl_op)

    if ('phase' in data_selection and 'amplitude' in data_selection):
        return reduce(add, lhs_phi) + reduce(add, lhs_rho)
    elif 'phase' in data_selection:
        return reduce(add, lhs_phi)
    elif 'amplitude' in data_selection:
        return reduce(add, lhs_rho)
    else:
        print('ERROR: invalud data choice in likelihood construction.')
        exit()

def likelihood_t3p(data, eps=1e-8):
    """
    Construct the combined closure phase likelihood for all available triangles.
    It acts on a MultiField with one entry per baseline. The naming convention in the MultiField is bl-{idx_tel1}-{idx_tel2}.
    For the usual GRAVITY observations the telescope indices run from 0 to 3.
    
    Parameters:
    -----------
    data (dict)      : GRAVITY data dictionary as returned by read data
    eps (float, opt) : fudge-factor to exclude division by zero (irrelevant in the end because these data points 
                       are flagged anyway)

    Returns:
    --------
    lh (ift.Operator) : the closure likelihood operator
    """

    lh_t3p = []
    for icl, ccl in enumerate(data['closures']):

        # get the mask operator
        masks = _closure_mask(data, icl)
        csets = ift.ContractionOperator(masks[0].domain, spaces=0)

        # data, here we compute the distance between two unit vectors in the complex plane
        # which is equivalent to the phase difference for small errors and downweights extreme outliers
        # note that the three masks are identical w.r.t. individual data points
        data_t3p = np.exp(1j*data['t3p'][:,icl,:,:])
        # repeat in the direction of nsets
        data_t3p = np.repeat(data_t3p[np.newaxis,:,:,:], 1+int(np.sum(data['closures']==0)), axis=0)
        # recollapse, using the 1st mask
        data_t3p = (csets @ masks[0])(ift.makeField(masks[0].target, data_t3p))

        # inverse covariance matrix
        cov_t3p = 1./(data['t3e'][:,icl,:,:]**2. + eps)
        cov_t3p = np.repeat(cov_t3p[np.newaxis,:,:,:], 1+int(np.sum(data['closures']==0)), axis=0)
        cov_t3p = (csets @ masks[0])(ift.makeField(masks[0].target, cov_t3p))
        cov_t3p = ift.makeOp(cov_t3p)

        # translate visibilities to closures (on the unit circle)
        bl_names = ['bl-{:d}-{:d}'.format(*data['baselines'][ibl]) for ibl in data['closures'][icl] ]
        uc_op    = ToUnitCircle(csets.domain)
        cl_op = (csets @  masks[0] @ uc_op @ ift.FieldAdapter(masks[0].domain, bl_names[0]) ) \
              * (csets @  masks[1] @ uc_op @ ift.FieldAdapter(masks[1].domain, bl_names[1]) ) \
              * (csets @  masks[2] @ uc_op @ ift.FieldAdapter(masks[2].domain, bl_names[2]).conjugate() )
        
        lh_op = ift.GaussianEnergy(mean=data_t3p, inverse_covariance=cov_t3p) @ cl_op
        lh_t3p.append(lh_op)

    return reduce(add, lh_t3p)


class ToUnitCircle(ift.Operator):
    def __init__(self, domain):
        self._domain = self._target = ift.DomainTuple.make(domain)

    def apply(self, x):
        return x*((x*x.conjugate()).real.sqrt().reciprocal())

class ToAmplitude(ift.Operator):
    def __init__(self, domain):
        self._domain = self._target = ift.DomainTuple.make(domain)

    def apply(self, x):
        return (x*x.conjugate()).real.sqrt()
