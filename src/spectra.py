# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift

def _lambda_values(data, res, ibl):
    """
    Get sub-sampling of channels on the specified baseline
    """
    res     = int(res)
    lambdas = np.full(data['nlam']*res, np.nan)
    stepping, step = np.linspace(-1 ,1 ,res, endpoint=False, retstep=True)
    stepping += step/2
    if res==1:
        stepping=0
    for ill, ll in enumerate(data['lam']):
        lambdas[res*ill:res*(ill+1)] = ll + data['bwl'][ill]/2.*stepping
    return lambdas

def make_variable_spectrum_op(data, res, ibl, dom_sky, alpha_key, use_closure=False):
    """
    Create an operator to model power-law spectra w/ flexible spectral index.

    Parameters:
    -----------
    data (dict)               : GRAVITY data as returned by read_data
    res (int)                 : Number of sub-gridding points per spectral channel to approximate wavelength smearing
    ibl (int)                 : Baseline index
    dom_sky (ift.MultiDomain) : domain to which the sky model projects
    alpha_key (string)        : name of relevant excitation in the full ift.MultiFIeld
    use_closure (bool, opt.)  : indicate wheather the model should account for closure phases (default=False)
    """
    # domains
    # to determine how often a baseline appears in the closure triangles we use the 0th baseline and assume its the same for all others
    n_sets    = 1 + int(use_closure)*int(np.sum(data['closures']==0))
    dom_alpha = ift.ContractionOperator(ift.UnstructuredDomain(data['nlam']), spaces=0).target
    dom_lam   = [ift.UnstructuredDomain(n_sets), ift.UnstructuredDomain(data['nframe']), ift.UnstructuredDomain(res*data['nlam'])]
    dom_lam   = ift.DomainTuple.make(dom_lam)

    # wavelength values
    # -> broadcast in the direction of sets and frames
    lambdas   = _lambda_values(data, res, ibl)/2.2e-6
    lambdas   = np.repeat(lambdas[np.newaxis], data['nframe'], axis=0)
    lambdas   = np.repeat(lambdas[np.newaxis], n_sets, axis=0)
    lambdas   = ift.makeField(dom_lam, lambdas)
    lambdas   = ift.makeOp(lambdas) @ ift.NullOperator(dom_alpha, dom_lam).exp() @ ift.ducktape(dom_alpha, dom_sky, alpha_key)

    # spectral index
    alpha     = ift.ducktape(dom_alpha, dom_sky, alpha_key)
    expop     = ift.ContractionOperator(dom_lam, spaces=None).adjoint
    adder     = ift.Adder(ift.full(dom_alpha, 1.))
    alpha     = expop @ adder @ alpha

    #return the power law
    return (-alpha*lambdas.log()).exp()

