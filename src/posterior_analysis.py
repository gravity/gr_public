# GRAVITY_RESOLVE (G^R) - GRAVITY imaging with Information Field Theory
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2019-2021 Max-Planck-Society
# Author: Julia Stadler

import numpy as np
import nifty7 as ift
import matplotlib.pyplot as plt
from matplotlib.colors import rgb2hex
import matplotlib.colors as colors
import re

from .output import lighten_color, get_uv_coordinates
from .output import set_style
from .constants import MAS2RAD
from .likelihood import _data_mask, _closure_mask, ToUnitCircle

def plot_brightness_evolution_samples(operators, klpos, klsamp, tmax, times=[],\
                                      outname=None, pos_true=None, rescale=False, lc_true=None,\
                                      ylabel=None, ylim=None):
    """
    Plot the lightcurve of all samples together with the sample mean.

    Parameters:
    -----------
    operators (list) : operators to compute the lightcurves from excitations
    klpos (ift.Multifield) : current position
    klsamp (list) : ift.Multifields for the samples
    tmax (float) : time span to consider in the plot
    times (list, opt.) : time coordinates corresponding to each value in the lightcurve
                         if not provided constant spacing between 0 and tmax is assumed
    outname (string, opt) : save the plot in .png format
    pos_true (ift.MultiField, opt.) : excitations to generate the true lightcurve which will be over-plotted
                                      (e.g. for working with mock data)
    lc_true (ift.MultiField, opt.) : true lightcurve which will be overplotted (e.g. for working with mock data)
    rescale (bool, opt.) : global scaling to match inferred to true lightcurves
    ylabel (string, opt.) : label y-axis
    ylim (tuple, opt.) : limit extend of the y-axis

    Returns: the figure object
    -------
    """
    
    set_style('plotstyle')
    #colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    colors = ['steelblue', 'dimgrey']
    rescale_fac = [float(rescale)]*len(operators)
    
    pltargs = {}
    if isinstance(operators[0].target[0], ift.RGSpace):
        tdat = np.linspace(0, tmax, operators[0].target.shape[0])
    elif times!=[]:
        tdat = times
        pltargs['linestyle'] = 'None'
        pltargs['marker']    = 'o'
    else:
        tdat = np.arange(0, operators[0].target[0].shape[0], 1.)

    # detect large gaps in the time array
    dtime = times[1:]-times[:-1]
    breakes = np.argwhere(dtime > 60.).flatten()
    nax = len(breakes) + 1
    limits = np.zeros( [nax,2])
    for ib in range(nax-1):
        limits[ib,1]   = tdat[breakes[ib]] + 5.0
        limits[ib+1,0] = tdat[breakes[ib]+1] -5.0
    limits[-1,1] = tdat[-1] + 5.0
    limits[0,0] = -5.0

    fig, ax = plt.subplots(1, nax, sharey=True)
    ax = np.array([ax, ]).flatten()
        
    for io, oo in enumerate(operators):
        sc   = ift.StatCalculator()
        for samp in klsamp:
            for aa in ax:
                aa.plot(tdat, oo.force(klpos + samp).val, c=lighten_color(colors[io], 0.3))
            sc.add(oo.force(klpos + samp))
        for aa in ax:
            aa.plot(tdat, sc.mean.val, c=colors[io], **pltargs)

        # also plot the true curve and possibly rescale it
        true_curve = None
        if np.all(lc_true!=None):
            true_curve = lc_true[io].val
        if isinstance(pos_true, ift.MultiField):
            true_curve = oo.force(pos_true).val
        if np.all(true_curve!=None) and rescale!=False:
            if rescale_fac[io] == 1:
                rescale_fac[io] = 1./np.mean(true_curve/sc.mean.val)
            true_curve = true_curve*rescale_fac[io]
        if np.all(true_curve!=None):
            for aa in ax:
                aa.plot(tdat, true_curve, c=lighten_color(colors[io], 1.5), ls='-')

    # horizontal lines to indicate frames
    if len(times)>0:
        for aa in ax:
            [aa.axvline(tt, lw=.5, linestyle='--') for tt in times]

    # customized axis limits
    if ylim!=None:
        for aa in ax:
            aa.set_ylim(ylim)

    # multi-plot formatting
    if nax>1:
        d = .015 
        for ia, aa in enumerate(ax):
            aa.set_xlim(*limits[ia])
            kwargs = dict(transform=aa.transAxes, color='k', clip_on=False)
            if ia<nax-1:
                aa.spines['right'].set_visible(False)
                aa.yaxis.tick_left()
                aa.plot((1-d,1+d), (-d,+d), **kwargs)
                aa.plot((1-d,1+d),(1-d,1+d), **kwargs)
            if ia>0:
                aa.spines['left'].set_visible(False)
                aa.yaxis.tick_right()
                aa.plot((-d,+d), (1-d,1+d), **kwargs)
                aa.plot((-d,+d), (-d,+d), **kwargs)
        fig.tight_layout()

    # labels
    if ylabel==None:
        ylabel = 'central brightness [arbitrary units]'
    ax = fig.add_subplot(111, frameon=False)
    ax.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    ax.set_xlabel('observation time [min]')
    ax.set_ylabel(ylabel)
    if rescale != False:
        msg = 'true curves rescaled by {:.2f}'.format(rescale_fac[0])
        for ff in rescale_fac[1:]:
            msg+= ', {:.2f}'.format(ff)
        ax.text(0.95, 0.95, msg, transform=ax.transAxes, va='top', ha='right', backgroundcolor='w')
    
    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig


def plot_image_samples(stats_obj, fov, outname=None, sourcelist=[], fiber_fwhm=None, extra_ps=[],\
                       vmin=None, vmax=None, figsize=None, fontsize=None):

    """
    Visualizes the point sources detected and their significange. For details see Appendix E of the imaging paper.
    
    Parameters:
    -----------
    stats_obj (ift.StatCalculator) : constining statistics on the image of faint sources
    fov (float) : spatial extend of the plot
    sourcelist (list) : positions where sources are expected, for each source give (ra,dec)
    extra_ps (list, opt) : list of additional point sources to be overplotted
                             for each source give ((ra,std),(dec,std),(flux,std))
    fiber_fwhm (float, opt) : fiber FWHM to be overplotted
    vmin (float, opt.) : lower cut on the color sclae
    vmax (float, opt.) : higher cut on the color scale
    figsize (float, opt.) : size of the figure in inches
    fontsize (float, opt.) : fontsize

    Returns: the figure object
    --------
    
    """
    
    set_style('plotstyle')
    if figsize!=None:
        plt.rcParams['figure.figsize']=figsize
    if fontsize!=None:
        plt.rcParams['font.size']=fontsize
    fig, ax = plt.subplots(1)

    # plot an approximation for the fiber profile in the bkg
    if fiber_fwhm != None and fov != None:
        npix     = stats_obj.mean.shape[0]
        sig      = fiber_fwhm/2./np.sqrt(2.*np.log(2.))
        xx, yy   = np.meshgrid(np.linspace(-fov/2, fov/2, npix), np.linspace(-fov/2, fov/2, npix))
        fiber    = np.exp( -1.*(xx**2 + yy**2)/2./sig**2)
        cs       = ax.contour(xx, yy, fiber, colors='grey', alpha=.3, levels=[0.01, 0.1, 0.5])
        cs_labels = ax.clabel(cs, inline=True, fontsize='small', rightside_up=True, fmt='%1.2f')
        for ll in cs_labels:
            ll.set_rotation(0.)
        ax.text(0.95, 0.03, 'FWHM = {:.1f} mas'.format(fiber_fwhm), transform=ax.transAxes, c='grey', fontsize='small', va='bottom', ha='right')

    # set axis limits
    if fov != None:
        fov_ra  = [fov/2, -fov/2]
        fov_dec = [-fov/2, fov/2]  
        if len(extra_ps) > 0:
            fov_ra  = [max(fov_ra[0], extra_ps[:,0,0].max()+5.), min(fov_ra[1], extra_ps[:,0,0].min()-5.)]
            fov_dec = [min(fov_dec[0], extra_ps[:,1,0].min()-5.), max(fov_dec[1], extra_ps[:,1,0].max()+5.)]
        ax.set_xlim(fov_ra)
        ax.set_ylim(fov_dec)
        ax.set_xlabel('RA [mas]')
        ax.set_ylabel('DEC [mas]')
    else:
        dim = stats_obj.mean.shape[0]/2
        ax.set_xlim([dim, -dim])
        ax.set_ylim([-dim, dim])
        ax.set_xlabel('RA [pixel]')
        ax.set_ylabel('DEC [pixel]')
    ax.set_aspect('equal')
 
    # extract the source brightness
    image1 = stats_obj.mean.val.copy()
    
    # extract source coordinates and flux
    spix      = stats_obj.mean.domain[0].distances[0]
    fov       = stats_obj.mean.domain[0].shape[0]*spix
    thresh    = image1.mean()*10
    mask      = image1 < thresh
    src_flux  = image1[~mask]
    src_coord = np.argwhere(image1 > thresh)*spix - fov/2
    if vmin == None:
        vmin      = min(src_flux)
    if vmax==None:
        vmax      = max(src_flux)
        if len(extra_ps) > 0:
            vmax = max([vmax, extra_ps[:,2,0].max()]) 
    
    # process source variance
    image2 = 1./np.sqrt(stats_obj.var.val.copy())
    image2[mask] = 0.
    image2 = image1*image2
    
    src_var = image2[~mask]
    thr_var = 10.

    # set the overall marker size in the plot
    sizes     = np.clip(src_var, None, thr_var)
    if len(extra_ps) > 0:
        sizes2 = np.clip(extra_ps[:,2,0]/(extra_ps[:,2,1]+1.e-6), None, thr_var)
        size_norm = 200./np.max(np.concatenate((sizes, sizes2)))
    else:
        size_norm = 200./max(sizes)
    
    # plot the mean image & variance
    sizes     = size_norm*sizes 
    im1 = ax.scatter(src_coord[:,0], src_coord[:,1], s=sizes, c=src_flux, cmap='Blues', marker='o',\
                     norm=colors.LogNorm(vmin=vmin, vmax=vmax), edgecolor='k', linewidth=.5)

    # plot external sources
    if len(extra_ps) > 0:
        sizes2 = size_norm*sizes2
        ax.scatter(extra_ps[:,0,0], extra_ps[:,1,0], c=extra_ps[:,2,0], s=sizes2, cmap='Blues', marker='o',\
                   norm=colors.LogNorm(vmin=vmin, vmax=vmax), edgecolor='k', linewidth=.5)
        ax.errorbar(extra_ps[:,0,0], extra_ps[:,1,0], xerr=extra_ps[:,0,1], yerr=extra_ps[:,1,1], c='lightgrey', ls='None', marker='.', ms=0)
    
    # add expected sources
    if len(sourcelist) > 0:
        sourcelist = np.array(sourcelist)
        src_x = sourcelist[:,0]
        src_y = sourcelist[:,1]
        ax.scatter(src_x, src_y, marker='x', c='r')

    
    # label the significance
    # labels = np.linspace(np.floor(np.max([2., src_var.min()])), np.ceil(src_var.max()), 4)
    #labels = np.linspace(src_var.min(), np.min([th_img, thr_var]), 4)
    labels = np.linspace(src_var.min(), np.min([src_var.max(), thr_var]), 4)
    handels, labels = im1.legend_elements(prop='sizes', num=list(labels*size_norm), c='w', markeredgecolor='k',\
                                    markeredgewidth=.5)
    labels_num = [float(re.findall(r'\d+', ll)[0])/size_norm for ll in labels]
    labels     = ['$\sigma = {:.1f}$'.format(ll) for ll in labels_num]
    if np.max(src_var) > 10:
        labels[-1] = '$\sigma > {:.1f}$'.format(labels_num[-1])
    ax.legend(handels, labels, labelspacing=1.5, loc='lower left')
        
    fig.tight_layout()

    cb = fig.colorbar(im1, orientation='vertical')

    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig

def get_residuals_dict(vis, data, err_scale={}):
    """
    Create a dictionary with residuals of visbility real and imaginary part, phase and amplitude and closure phase. 
    Entries have the following dimensions:
    visibilities -> nbl x nframe x npol x nlam
    closures -> ncl x nframe x npol x nlam

    Parameters:
    -----------
    * vis (ift.MultiField) : visibilities as predicted by the model
    * data (dictionary) : the data dictionary as returned by src.data.read_data

    Returns:
    --------
    res_dict : the residuals dictionary
    """
    # for all data types where there is no error scale provided set it to 1
    err_scale_int   = {}
    err_scale_names = ['phase', 'amplitude', 'closure']
    for kk in err_scale_names:
        if kk not in err_scale.keys():
            err_scale_int[kk] = 1.
            #print('WARNING: no error scale provided for {}, using 1 instead.'.format(kk))
        else:
            err_scale_int[kk] = err_scale[kk]
    for kk in err_scale.keys():
        if kk not in err_scale_names:
            print('WARNING: invalid error scale ({}) passed to get_residuals_dict, the value will be ignores.'.format(kk))
    
    
    # check if the model contains closure phases
    use_closure = vis['bl-{}-{}'.format(*data['baselines'][0])].val.shape[0] == 3

    # obtain data masks
    masks_vis = [_data_mask(data, ibl, False) for ibl in range(data['nbl'])]
    masks_vis = [mm(ift.full(mm.domain, 1.)).val[0] for mm in masks_vis]
    masks_vis = np.array(masks_vis)
    masks_t3p = [_closure_mask(data, icl) for icl in range(data['ncl'])]

    # compute residuals connected to visibilities
    res_rea = np.zeros([data['nbl'], data['nframe'], data['npol'], data['nlam']]) 
    res_img = np.zeros([data['nbl'], data['nframe'], data['npol'], data['nlam']]) 
    res_rho = np.zeros([data['nbl'], data['nframe'], data['npol'], data['nlam']]) 
    res_phi = np.zeros([data['nbl'], data['nframe'], data['npol'], data['nlam']])
    
    for ibl in range(data['nbl']):
        bl_name = 'bl-{:d}-{:d}'.format(*data['baselines'][ibl])
        res_rea[ibl] = vis[bl_name].real.val[0] - np.real(data['rho'][:,ibl,:,:]*np.exp(1j*data['phi'][:,ibl,:,:]))
        res_img[ibl] = vis[bl_name].imag.val[0] - np.imag(data['rho'][:,ibl,:,:]*np.exp(1j*data['phi'][:,ibl,:,:]))
        res_phi[ibl] = np.angle(vis[bl_name].val[0]/np.abs(vis[bl_name].val[0])*np.exp(-1j*data['phi'][:,ibl,:,:]))
        res_rho[ibl] = np.abs(vis[bl_name].val[0]) - data['rho'][:,ibl,:,:]
        # noise weighting
        # assuming the convex lh approximation for real and imaginary part
        res_rea[ibl] = res_rea[ibl]/(data['sro'][:,ibl,:,:]+1e-8)
        res_img[ibl] = res_img[ibl]/(data['rho'][:,ibl,:,:]+1e-8)/(data['spi'][:,ibl,:,:]+1e-8)
        res_rho[ibl] = res_rho[ibl]/(data['sro'][:,ibl,:,:]+1e-8)/err_scale_int['amplitude']
        res_phi[ibl] = res_phi[ibl]/(data['spi'][:,ibl,:,:]+1e-8)/err_scale_int['phase']
        
    # exclude masked data from the plot
    res_rea[masks_vis==0] = np.nan
    res_img[masks_vis==0] = np.nan
    res_rho[masks_vis==0] = np.nan
    res_phi[masks_vis==0] = np.nan

    res_dct = {'res_rea':res_rea, 'res_img':res_img, 'res_rho':res_rho, 'res_phi':res_phi}
    if not use_closure:
        return res_dct

    # compute closure residuals
    res_t3p = np.zeros([data['ncl'], data['nframe'], data['npol'], data['nlam']])
    for icl, ccl in enumerate(data['closures']):
        bl_names = ['bl-{:d}-{:d}'.format(*data['baselines'][ibl]) for ibl in ccl ]
        csets = ift.ContractionOperator(vis[bl_names[0]].domain, spaces=0)
        uc_op    = ToUnitCircle(csets.domain)

        cl_op = (csets @ masks_t3p[icl][0] @ uc_op @ ift.FieldAdapter(masks_t3p[icl][0].domain, bl_names[0]) )\
               *(csets @ masks_t3p[icl][1] @ uc_op @ ift.FieldAdapter(masks_t3p[icl][1].domain, bl_names[1]) )\
               *(csets @ masks_t3p[icl][2] @ uc_op @ ift.FieldAdapter(masks_t3p[icl][2].domain, bl_names[2]).conjugate() )
        
        # closure phase
        t3p   = cl_op.force(vis).val
        #t3p   = t3p/np.abs(t3p)
        # residual
        res_t3p[icl] = np.angle(t3p * np.exp(-1j*data['t3p'][:,icl,:,:]))
        # noise weighting
        res_t3p[icl] = res_t3p[icl]/data['t3e'][:,icl,:,:]/err_scale_int['closure']
        # exclude masked data
        idx_set = np.argwhere(np.all(data['closures'][np.any(data['closures']==ccl[0], axis=-1)] ==ccl, axis=-1))[0,0] + 1
        mask    = masks_t3p[icl][0](ift.full(masks_t3p[icl][0].domain, 1.)).val[idx_set]
        res_t3p[icl, mask==0] = np.nan
        
    res_dct['res_t3p'] = res_t3p
    return res_dct
    
def plot_residuals_time(vis, data, data_selection, lh_type, lightcurve=None, outname=None, polarization=None):
    
    """
    Plot the noise-weighted residuals over time

    Parameters:
    -----------
    vis               : ift.MultiFIeld with the visibilities per baseline. Ideally the mean of a ift.StatsCreator() object
    data              : an instance of the gravity data directory
    data_selction     : list of data types considered in the imaging
    lh_type           : likelihood formulation used, which determines the type of data plotted
    lightcurve (opt.) : list of fields containing the lightcurves for each polarization state.
    outname           : file to save to plot to. if none the plot is not saved
    polarization (int) : select a polarization state

    Returns: the figure object
    -------

    """

    use_closure = 'closure' in data_selection
    set_style('plotstyle')
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    markers = ['.', 'x']

    # get residuals
    res_dct   = get_residuals_dict(vis, data)

    # select polarization
    polrange = range(data['npol'])
    if polarization!=None:
        polrange = [polarization]
        
    # if lh_type=convex_approx, display real- and imaginary part residuals alongside phase and amplitue residuals
    # otherwise show residuals of the data types used
    if lh_type=='convex_approx':
        fig, ax = plt.subplots(2,2, sharex=True)
        ax = ax.flatten()
    else:
        # detect large gaps in the time array
        dtime = data['tim'][1:]-data['tim'][:-1]
        breakes = np.argwhere(dtime > 60.).flatten()
        nax = len(breakes) + 1
        limits = np.zeros( [nax,2])
        for ib in range(nax-1):
            limits[ib,1]   = data['tim'][breakes[ib]] + 5.0
            limits[ib+1,0] = data['tim'][breakes[ib]+1] -5.0
        limits[-1,1] = data['tim'][-1] + 5.0
        limits[0,0] = -5.0

        fig, ax = plt.subplots(len(data_selection), nax, sharey=True)
        if len(ax.shape)==1:
            ax = np.expand_dims(ax, axis=-1)
    
    times = np.repeat(data['tim'][:, np.newaxis], data['nlam'], axis=-1)
    for ibl in range(data['nbl']):
        for ip in polrange:
            idx_pp = 0
            if lh_type=='convex_approx':
                ax[0].scatter(times, res_dct['res_rea'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[1].scatter(times, res_dct['res_img'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[0].set_title('Re(VIS), nw. residual')
                ax[1].set_title('Im(VIS), nw. residual')
                ax[-2].set_xlabel('time [min]')
                idx_pp += 2
            if 'phase' in data_selection:
                for aa in ax[idx_pp]:
                    aa.scatter(times, res_dct['res_phi'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[idx_pp,0].set_ylabel('VISPHI, nw. residual')
                idx_pp += 1
            if 'amplitude' in data_selection:
                for aa in ax[idx_pp]:
                    aa.scatter(times, res_dct['res_rho'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[idx_pp,0].set_ylabel('VISAMP, nw. residual')
                idx_pp += 1

    for icl in range(data['ncl']):
        for ip in polrange:
            if 'closure' in data_selection:
                for aa in ax[idx_pp]:
                    aa.scatter(times, res_dct['res_t3p'][icl,:,ip,:], c=colors[data['nbl']+icl], marker=markers[ip])
                ax[idx_pp,0].set_ylabel('T3P, nw. residual')
            
    if lightcurve != None:
        max_lc = np.max([lc.val.max() for lc in lightcurve])
        tmax   = lightcurve[0].shape[0]*lightcurve[0].scalar_weight(spaces=0)
        npix_t = lightcurve[0].shape[0]
        for aa in ax:
            lc_norm  = aa.get_ylim()[1]/max_lc*2
            lc_shift = aa.get_ylim()[1]
            for lc in lightcurve:
                aa.plot(np.linspace(0, tmax, npix_t), lc.val*lc_norm-lc_shift, c='k')

    # multi-plot formatting
    if nax>1:
        d = .015
        for jj in range(ax.shape[0]):
            for ia, aa in enumerate(ax[jj]):
                aa.set_xlim(*limits[ia])
                kwargs = dict(transform=aa.transAxes, color='k', clip_on=False)
                if ia<nax-1:
                    aa.spines['right'].set_visible(False)
                    aa.yaxis.tick_left()
                    aa.plot((1-d,1+d), (-d,+d), **kwargs)
                    aa.plot((1-d,1+d),(1-d,1+d), **kwargs)
                if ia>0:
                    aa.spines['left'].set_visible(False)
                    aa.yaxis.tick_right()
                    aa.plot((-d,+d), (1-d,1+d), **kwargs)
                    aa.plot((-d,+d), (-d,+d), **kwargs)

    fig.tight_layout()
    
    # labels
    ax = fig.add_subplot(111, frameon=False)
    ax.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    ax.set_xlabel('observation time [min]')

    
    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig



def plot_residuals_uv(vis, data, data_selection, lh_type, outname=None):
    
    """
    Plot the noise weighted residuals over spatial frequency.

    Parameters:
    -----------
    vis           : ift.MultiFIeld with the visibilities per baseline. Ideally the mean of a ift.StatsCreator() object
    data          : an instance of the gravity data directory
    data_selction : list of data types considered in the imaging
    lh_type       : likelihood formulation used, which determines the type of data plotted
    outname       : file to save to plot to. if none the plot is not saved

    """
    use_closure = 'closure' in data_selection
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    set_style('plotstyle')
    markers = ['.', 'x']

    # get uv-coordinates
    uv_vis, uv_t3p = get_uv_coordinates(data)

    # get residuals
    res_dct   = get_residuals_dict(vis, data)

    # if lh_type=convex_approx, display real- and imaginary part residuals alongside phase and amplitue residuals
    # otherwise show residuals of the data types used
    if lh_type=='convex_approx':
        fig, ax = plt.subplots(2,2, sharex=True)
    else:
        fig, ax = plt.subplots(len(data_selection), sharex=True)
    ax = ax.flatten()

    for ibl in range(data['nbl']):
        for ip in range(data['npol']):
            idx_pp = 0
            if lh_type=='convex_approx':
                ax[0].scatter(uv_vis[:,ibl,:], res_dct['res_rea'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[1].scatter(uv_vis[:,ibl,:], res_dct['res_img'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[0].set_title('Re(VIS), nw. residual')
                ax[1].set_title('Im(VIS), nw. residual')
                ax[-2].set_xlabel('spatial frequency [1/as]')
                idx_pp += 2
            if 'phase' in data_selection:
                ax[idx_pp].scatter(uv_vis[:,ibl,:], res_dct['res_phi'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[idx_pp].set_title('VISPHI, nw. residual')
                idx_pp += 1
            if 'amplitude' in data_selection:
                ax[idx_pp].scatter(uv_vis[:,ibl,:], res_dct['res_rho'][ibl,:,ip,:], c=colors[ibl], marker=markers[ip])
                ax[idx_pp].set_title('VISAMP, nw. residual')
                idx_pp += 1

    for icl in range(data['ncl']):
        for ip in range(data['npol']):
            if 'closure' in data_selection:
                ax[idx_pp].scatter(uv_t3p[:,icl,:], res_dct['res_t3p'][icl,:,ip,:], c=colors[data['nbl']+icl], marker=markers[ip])
                ax[idx_pp].set_title('Closure phase, nw. residual')
    ax[-1].set_xlabel('spatial frequency [1/as]')

    fig.tight_layout()            
    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig

def plot_residuals_channel(model, data, data_selection, outfile=None, ylim=None):

    """
    Plot residuals over the spectra channel.

    model (ift.MultiFIeld) : the predicted visbibilities
    data (dictionary) : data dictionary as returned by src.data.get_data
    data_selection (list) : data types considered in the imaging
    outfile (string, opt.) : location to save the plot in .png format
    ylim (tuple, opt.) : limit y-axis range
    """
    set_style('plotstyle')
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    fs = plt.rcParams['figure.figsize']
    
    res_dct = get_residuals_dict(model, data)
    res_keys = {'phase':'res_phi', 'amplitude':'res_rho','closure':'res_t3p'}
    markers  = ['o', 'P', 'X']
    
    lam = np.repeat((data['lam']*1e6)[np.newaxis,:], data['nframe'], axis=0)
    lam = np.repeat(lam[:,np.newaxis,:], data['npol'], axis=1)
    
    fig, ax = plt.subplots(int(data['nbl']/2), 2, sharex=True, sharey=True, figsize=(fs[0], fs[1]))
    ax = ax.flatten()
    for ibl in range(data['nbl']):
        for idd, ds in enumerate(data_selection):
            if ds=='phase' or ds=='amplitude':
                c = rgb2hex(lighten_color(colors[ibl], idd/len(data_selection)+0.5))
                ax[ibl].scatter(lam+0.01*idd, res_dct[res_keys[ds]][ibl], c=c, marker=markers[idd], label=ds)
            else:
                mask = np.array([ibl in data['closures'][icl] for icl in range(data['ncl'])])
                icls = np.argwhere(mask==True).flatten()
                res = [mask]
                for icl in icls:
                    c = rgb2hex(lighten_color(colors[icl+6], idd/len(data_selection)+0.5))
                    ax[ibl].scatter(lam+0.01*idd, res_dct[res_keys[ds]][icl], c=c, marker=markers[idd], label=ds)

    if ylim!=None:
        for aa in ax:
            aa.set_ylim(ylim)
    ax[-1].legend(ncol=3)
    ax2 = fig.add_subplot(111, frameon=False)
    ax2.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
    ax2.set_xlabel('wavelength $\lambda$ [$\mu$m]')
    ax2.set_ylabel('noise weighted residuals')

    fig.tight_layout()
    plt.show()
    if outfile!=None:
        fig.savefig(outfile)
    plt.close()

def plot_residuals_correlation(vis, data, data_selection, outname=None):

    """
    Generate a scatter plot showning phase- vs. amplitude residuals.

    Parameters:
    -----------
    vis (ift.MultiField) : visibilities as predicted by the model
    data (dictionary) : data dictionary as returned by src.data.read_data
    data_selection (list) : which data types where considered

    Returns: the figure object
    --------
    """
    use_closure = vis['bl-{}-{}'.format(*data['baselines'][0])].val.shape[0] == 3
    set_style('plotstyle')
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
    fs = plt.rcParams['figure.figsize']
    ds_tmp = data_selection.copy()
    ds_tmp.remove('amplitude')
    
    
    res_dct   = get_residuals_dict(vis, data)
    
    for ds in ds_tmp:
        fig, ax = plt.subplots(2, int(data['nbl']/2), sharex=True, sharey=True)
        ax = ax.flatten() 
        for ibl in range(data['nbl']):
            if ds=='phase':
                resY = res_dct['res_phi'][ibl]
                resX = res_dct['res_rho'][ibl]
                c = np.array([lighten_color(colors[ibl], iff/data['nframe']+0.5) for iff in range(data['nframe'])])
                c = np.repeat(c[:,np.newaxis,:], data['npol'], axis=1)
                c = np.repeat(c[:,:,np.newaxis,:], data['nlam'], axis=2)
                c = c.reshape(-1, 3)
            elif ds=='closure':
                mask = np.array([ibl in data['closures'][icl] for icl in range(data['ncl'])])
                icls = np.argwhere(mask==True).flatten()
                resY = res_dct['res_t3p'][mask]
                resX= np.repeat(res_dct['res_rho'][ibl][np.newaxis], len(icls)) 
                c = np.array([[lighten_color(colors[icl+6], iff/data['nframe']+0.5) for iff in range(data['nframe'])]\
                                 for icl in icls])
                c = np.repeat(c[:,:,np.newaxis,:], data['npol'], axis=2)
                c = np.repeat(c[:,:,:,np.newaxis,:], data['nlam'], axis=3)
                c = c.reshape(-1,3)
            ax[ibl].scatter(resX, resY, c=c)
            ax[ibl].text(0.75, 0.9, 'bl-{}-{}'.format(*data['baselines'][ibl]), transform=ax[ibl].transAxes)
        ax2 = fig.add_subplot(111, frameon=False)
        ax2.tick_params(labelcolor='none', which='both', top=False, bottom=False, left=False, right=False)
        ax2.set_xlabel('noise weighted amplitude residual')
        ax2.set_ylabel('noise weighted {} residual'.format(ds))

        fig.tight_layout()
        plt.show()
        if outname != None:
            fig.savefig(outname+'_{}.png'.format(ds))

        plt.close()



def plot_residuals_hist(vis, data, data_selection, lh_type, outname=None, xmax=None):
    
    """
    Plot histograms of the noise weighted residuals.

    vis            : ift.MultiFIeld with the visibilities per baseline. Ideally the mean of a ift.StatsCreator() object
    data           : an instance of the gravity data directory
    data_selction  : list of data types considered in the imaging
    lh_type        : likelihood formulation used, which determines the type of data plotted
    outname (opt.) : file to save to plot to. if none the plot is not saved
    xmax (opt.)    : range of histogram

    """
    
    set_style('plotstyle')
    # if lh_type=convex_approx, display real- and imaginary part residuals alongside phase and amplitue residuals
    # otherwise show residuals of the data types used
    if lh_type=='convex_approx':
        fig, ax = plt.subplots(2,2, sharex=True)
    else:
        fig, ax = plt.subplots(1, len(data_selection), sharex=True)
    ax = ax.flatten()

    if xmax==None:
        xmax=2

    # get residuals
    res_dct   = get_residuals_dict(vis, data)

    x = np.linspace(-xmax, xmax, 100)
    y = np.exp(-1.*x**2/2)/np.sqrt(2.*np.pi)

    idx_pp = 0
    if lh_type=='convex_approx':
        ax[0].hist(res_dct['res_rea'][~np.isnan(res_dct['res_rea'])], density=True, range=[-xmax,xmax], bins=30)
        ax[1].hist(res_dct['res_img'][~np.isnan(res_dct['res_img'])], density=True, range=[-xmax,xmax], bins=30)
        ax[0].set_title('Re(VIS), nw. residual')
        ax[1].set_title('Im(VIS), nw. residual')
        idx_pp += 2
    if 'phase' in data_selection:
        ax[idx_pp].hist(res_dct['res_phi'][~np.isnan(res_dct['res_phi'])], density=True, range=[-xmax,xmax], bins=30)
        ax[idx_pp].set_title('VISPHI, nw. residual')
        idx_pp += 1
    if 'amplitude' in data_selection:
        ax[idx_pp].hist(res_dct['res_rho'][~np.isnan(res_dct['res_rho'])], density=True, range=[-xmax,xmax], bins=30)
        ax[idx_pp].set_title('VISAMP, nw. residual')
        idx_pp += 1
    if 'closure' in data_selection:
        ax[idx_pp].hist(res_dct['res_t3p'][~np.isnan(res_dct['res_t3p'])], density=True, range=[-xmax,xmax], bins=30)
        ax[idx_pp].set_title('Closure phase, nw. residual')

    for aa in ax:
        aa.plot(x, y, c='k')

    fig.tight_layout()
    if outname != None:
        fig.savefig(outname)
    plt.close()
    return fig
