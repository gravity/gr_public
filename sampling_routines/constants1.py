def const_during_minimization(nit, pos):

    consts = []

    # remove degeneracies in the solution by fixing the OPD on one telescope to zero
    # this coice maintaings generality w.r.t the systematic OPD on all baselines
    # Sebastiano says that UT2 works best, so fix the OPD there
    if 'xi_opd_tel2' in pos.keys():
        consts.append('xi_opd_tel2')

    return consts
