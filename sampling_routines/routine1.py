def sampling_routine(nit):
    if nit<3:
        n_samp = 3
        n_it   = 4
    elif nit<5:
        n_samp = 5
        n_it   = 5
    elif nit<8:
        n_samp = 6
        n_it   = 8
    elif nit<12:
        n_samp  = 8
        n_it    = 10
    elif nit<20:
        n_samp   = 12
        n_it     = 15
    elif nit<25:
        n_samp   = 15
        n_it     = 20
    elif nit<30:
        n_samp   = 20
        n_it     = 30
    else:
        n_samp   = 20
        n_it     = 200
    return n_samp, n_it
