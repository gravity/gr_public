#---------- parameters to be defined by the user ----------
#----------------------------------------------------------

# 1. Pixelization
#################

# number of pixels along each spatial axis
npix_s = 256

# size of one pixel in mas
spix = 0.8

# maximum time (from first file) in minutes
# this is only used for plotting
tmax = 221

# 2. Data Selection
###################

# select spectral channels (max channel included)
min_channel = 3
max_channel = 12

# select polarisation state
# options are 'P1', 'P2', 'P12'
polarization = 'P12'

# exclude frames by their index in the whole data set
# this is applied BEFORE the time cuts below
idx_frame_exclude = []

# time range
# minimum and maximum time after the first frame was taken to consider in minutes
# you can set either limit to None to choose all avalable data
time_range = (None, None)

# upscale the error bars by a global factor
# set to 1 to disable
sigma_phi_scale = 1.0
sigma_amp_scale = 1.0
sigma_t3p_scale = 1.0

# choose which data to consider, valid options are phase, amplitude, closure
# note that using phases and closures simultanously is not consistant from an information theory prespective
data_selection = ['closure', 'amplitude']

# which form of the likelihood should be used
# 'convex_approx' or 'phase_amplitude', 'error_scaling'
lh_type = 'phase_amplitude'

# instrument name base
# extensions for polarizations will be added automatically if required
insname = 'GRAVITY_SC'

# 3. Sky Model
###############

# generative-invG model parameters for the faint-sources prior
invG_param = {'alpha' : 1.5,
             'q'      : 1.e-4,
             'delta'  : 0.001}

# spectral indices mean
alpha1 = -1.0
alpha2 =  2.0

# spectral index variances
alpha1_var = 3.
alpha2_var = 3.

# background flux level and spectral index
bkg_level    = 0.
bkg_var      = 0.
alpha_bg     = 2.0
alpha_bg_var = 0.

# variable point source -> location in [RA, Dec], standard deviation common to both directions and
# and overall scaling factor applied to the lightcurve
sgra_coord_mean  = [0., 0.]
sgra_coord_std   = 0.
sgra_point_scale = 1.0

# point sources with a gaussian prior on the position
# give mean in [RA, DEC] and a standard deviation common to both directions
# units are in mas
extra_ps_coord = [[2.8, -11.6],[29.2, 19.1],[-13.1, 13.1]]
extra_ps_sigma = [2.4, 2.4, 2.4]

# 4. Response Modelling
#######################

# bandwidth smearing
bw_resolution = 100

# model for phase maps
cmap_file = '20210520_gaussianmaps.fits'


# smoothing kernel for the phasemaps
# standard deviation in mas
phasemap_kernel_stddev = 10

# clip the phasemaps at some minimum value
# set to None to disable
phasemap_clip_low = None

# amplitude self-calibration
# if enabled, amplitude self-calibration is applied independently per frame and baseline
# choose between 'off', 'p-combined', 'p-split'
free_scale_bl       = True
free_scale_bl_sigma = 0.1

# 5. Initial position
#####################

# type of initial conditions, can be 'tuned', 'super-tuned', 'generic', 'super-tuned-sgra', 'all-flat'
# if tuned you also need to provide coordinates for S2
ini_type = 'prior-mode'

# restart the inference from an ealier run. set to None to disable
# the file is assumed to end in .npy, the 3 digits before the ending are taken as iteration number
restart_file = None


# 6. Minimization
#################
# explicitly set a random seed
# choose None to use default
random_seed = 7

# sampling routine & number of iterations
sampling_routine  = 1
n_iterations_mgvi = 30
constants_routine = 1

# iteration controller, choose from
# * gradient -> ift.GradientNormController
# * delta -> ift.DeltaEnergyController
iteration_controller = 'delta'

# minimizer, choose from
# * NwetonCG
# * VL_BFGS
minimizer_type ='NewtonCG'
#----------------------------------------------------------

import nifty7 as ift

image_domain     = ift.RGSpace(2*(npix_s,), distances=2*(spix,))
data_use_closure = 'closure' in data_selection
fov              = npix_s*spix
check_operators  = False

delta_it_ctrl_h = 1.e-1
delta_it_ctrl_m = 1.e-1
